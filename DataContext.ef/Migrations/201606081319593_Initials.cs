namespace DataContext.ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initials : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DaysOff",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Count = c.Double(nullable: false),
                        Unpaid = c.Boolean(nullable: false),
                        DayOffType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Birthday = c.DateTime(nullable: false),
                        MobilePhone = c.String(),
                        HiredAt = c.DateTime(nullable: false),
                        DismissAt = c.DateTime(),
                        JobPosition = c.String(),
                        Skype = c.String(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        Employee_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Employee_Id)
                .Index(t => t.Employee_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                        Employee_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.Employee_Id)
                .Index(t => t.Employee_Id);
            
            CreateTable(
                "dbo.Report",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        Hours = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        Employee_Id = c.Int(),
                        AppRole_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.Employee_Id)
                .ForeignKey("dbo.AspNetRoles", t => t.AppRole_Id)
                .Index(t => t.Employee_Id)
                .Index(t => t.AppRole_Id);
            
            CreateTable(
                "dbo.Salary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        Period = c.DateTime(nullable: false),
                        Rate = c.Int(nullable: false),
                        SalaryRateType = c.Int(nullable: false),
                        CalculationType = c.Int(nullable: false),
                        ReportedHours = c.Double(nullable: false),
                        WorkingDays = c.Double(nullable: false),
                        WorkingSum = c.Int(nullable: false),
                        VacationDays = c.Double(nullable: false),
                        VacationSum = c.Int(nullable: false),
                        SicknessDays = c.Double(nullable: false),
                        SicknessSum = c.Int(nullable: false),
                        Advance = c.Int(nullable: false),
                        OtherGrants = c.Int(nullable: false),
                        EvaluatedSalary = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.SalaryRates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplyFrom = c.DateTime(nullable: false),
                        RateType = c.Int(nullable: false),
                        Rate = c.Int(),
                        IsCurrent = c.Boolean(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Holidays",
                c => new
                    {
                        Day = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Day);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "AppRole_Id", "dbo.AspNetRoles");
            DropForeignKey("dbo.DaysOff", "EmployeeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalaryRates", "EmployeeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Salary", "EmployeeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "Employee_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Report", "EmployeeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "Employee_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "Employee_Id", "dbo.AspNetUsers");
            DropIndex("dbo.SalaryRates", new[] { "EmployeeId" });
            DropIndex("dbo.Salary", new[] { "EmployeeId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "AppRole_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "Employee_Id" });
            DropIndex("dbo.Report", new[] { "EmployeeId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "Employee_Id" });
            DropIndex("dbo.AspNetUserClaims", new[] { "Employee_Id" });
            DropIndex("dbo.DaysOff", new[] { "EmployeeId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Holidays");
            DropTable("dbo.SalaryRates");
            DropTable("dbo.Salary");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Report");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.DaysOff");
        }
    }
}
