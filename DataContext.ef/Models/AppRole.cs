﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataContext.ef.Models
{
   public class AppUserLogin : IdentityUserLogin<int> { }

    public class AppUserRole : IdentityUserRole<int> { }

    public class AppUserClaim : IdentityUserClaim<int> { }

    public class AppRole : IdentityRole<int, AppUserRole>, IRole<int> { }

    public class AppRoleStore : RoleStore<AppRole, int, AppUserRole>, IQueryableRoleStore<AppRole, int>, IRoleStore<AppRole, int>, IDisposable
    {
        public AppRoleStore() : base(new StuffDbContext())
        {
            base.DisposeContext = true;
        }

        public AppRoleStore(DbContext context)
            : base(context)
        {
        }
    }
}