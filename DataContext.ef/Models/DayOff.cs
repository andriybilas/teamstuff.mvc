﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.ef.Models
{
    public enum DayOffType
    {
        Vacation, 
        Sickness
    }

    [Table("DaysOff")]
    public class DayOff
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public double Count { get; set; }
        public bool Unpaid { get; set; }
        public DayOffType DayOffType { get; set; }
    }
}