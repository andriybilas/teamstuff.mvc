﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataContext.ef.Models
{
    public class Employee : IdentityUser<int, AppUserLogin, AppUserRole, AppUserClaim>, IUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string MobilePhone { get; set; }
        public DateTime HiredAt { get; set; }
        public DateTime? DismissAt { get; set; }
        public string JobPosition { get; set; }
        public string Skype { get; set; }
        public virtual IList<SalaryRate> SelaryRates { get; set; }
        public virtual IList<DayOff> Vacations { get; set; }
        public virtual IList<Salary> Salaries { get; set; }
        public virtual IList<Report> Reports  { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Employee, int> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

    }
}
