﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataContext.ef.Models
{
    public class Holiday
    {
        [Key]
        public DateTime Day { get; set; }
    }
}
