﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.ef.Models
{

    public enum CalculationType
    {
        Interim, 
        Complete
    }

    public class Salary
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public DateTime Period { get; set; }
        public int Rate { get; set; }
        public SalaryRateType SalaryRateType { get; set; }

        public CalculationType CalculationType { get; set; }

        public double ReportedHours { get; set; }
        public double WorkingDays { get; set; }
        public int WorkingSum { get; set; }

        public double VacationDays { get; set; }
        public int VacationSum { get; set; }

        public double SicknessDays { get; set; }
        public int SicknessSum { get; set; }

        public int Advance { get; set; }
        public int OtherGrants { get; set; }

        public int EvaluatedSalary { get; set; }
    }
}
