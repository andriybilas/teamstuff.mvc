﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.ef.Models
{
    public enum SalaryRateType
    {
        Hourly, 
        Monthly
    }

    public class SalaryRate
    {
        [Key]
        public int Id { get; set; }
        public DateTime ApplyFrom { get; set; }
        public SalaryRateType RateType { get; set; }
        public int? Rate { get; set; }
        public bool IsCurrent { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
