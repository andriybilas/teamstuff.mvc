﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataContext.ef.Models
{
    public class StuffDbContext : IdentityDbContext<Employee, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>
    {
        public StuffDbContext() : base("DefaultConnection")
        {
            Database.CreateIfNotExists();
        }
        public DbSet<SalaryRate> SelaryRates { get; set; }
        public DbSet<DayOff> DaysOff { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<Report> Reports { get; set; }

        public static StuffDbContext Create()
        {
            return new StuffDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<DayOff>().ToTable("dbo.DaysOff");
            builder.Entity<Employee>().ToTable("dbo.AspNetUsers");
            builder.Entity<Report>().ToTable("dbo.Report");
            builder.Entity<Salary>().ToTable("dbo.Salary");
            builder.Entity<SalaryRate>().ToTable("dbo.SalaryRates");
            builder.Entity<Holiday>().ToTable("dbo.Holidays");
            builder.Entity<AppUserRole>().ToTable("dbo.AspNetUserRoles");

            builder.Entity<AppUserRole>()
                .HasKey(r => new { r.UserId, r.RoleId })
                .ToTable("AspNetUserRoles");

            builder.Entity<AppUserLogin>()
                .HasKey(l => new { l.LoginProvider, l.ProviderKey, l.UserId })
                .ToTable("AspNetUserLogins");

            builder.Entity<AppUserClaim>().ToTable("dbo.AspNetUserClaims");

            builder.Entity<AppRole>().ToTable("dbo.AspNetRoles");
        }
    }
}
