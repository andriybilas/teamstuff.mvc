﻿using System.Security.Claims;
using System.Threading.Tasks;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace TeamStuff.mvc
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public interface IAppUserStore : IUserStore<Employee, int>
    {

    }

    public class AppUserManager : UserManager<Employee, int>
    {
        public AppUserManager(IAppUserStore store) : base(store)
        {

        }
    }

    public class AppUserStore : UserStore<Employee, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>, IAppUserStore
    {
        public AppUserStore() : base(new StuffDbContext())
        {

        }

        public AppUserStore(StuffDbContext context) : base(context)
        {

        }
    }
    public class ApplicationUserManager : UserManager<Employee, int>
    {
        public ApplicationUserManager(IUserStore<Employee, int> store) : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new AppUserStore(new StuffDbContext()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<Employee, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<Employee, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    public class ApplicationSignInManager : SignInManager<Employee, int>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(Employee user)
        {
            return user.GenerateUserIdentityAsync((AppUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
