using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using AutoMapper;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Ninject;
using Ninject.Web.Common;
using SharpRepository.Ioc.Ninject;
using SharpRepository.Repository.Ioc;
using TeamStuff.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.MapperPofiles;
using TeamStuff.mvc.Infrastructure.Services;

namespace TeamStuff.mvc.App_Start
{
    public class TeamStuffNinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public TeamStuffNinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            var mapperConfig = new MapperConfiguration(conf => {
                conf.AddProfile<EmployeeMapperProfile>();
                conf.AddProfile<SalaryMapperConfig>();
                conf.AddProfile<ProfileMapperConfig>();
            });

            _kernel.Bind<IMapper>().ToMethod(conf => mapperConfig.CreateMapper()).InRequestScope();
            _kernel.Bind<IEmploeeService>().To<EmploeeService>();
            _kernel.Bind<ISalaryRateService>().To<SalaryRateService>();
            _kernel.Bind<IDaysOffService>().To<DaysOffService>();
            _kernel.Bind<ISalaryService>().To<SalaryService>();
            _kernel.Bind<IProfileService>().To<ProfileService>();
            _kernel.Bind<IReportService>().To<ReportService>();

            _kernel.Bind<IUserStore<Employee, int>>().To<AppUserStore>();
            _kernel.Bind<IAuthenticationManager>().ToMethod(conf => HttpContext.Current.GetOwinContext().Authentication);

            _kernel.BindSharpRepository();
            RepositoryDependencyResolver.SetDependencyResolver(new SharpRepository.Ioc.Ninject.NinjectDependencyResolver(_kernel));
            _kernel.Bind<DbContext>().To<StuffDbContext>().InRequestScope().WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
    }
}
