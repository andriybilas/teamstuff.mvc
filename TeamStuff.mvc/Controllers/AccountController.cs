﻿using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity.Owin;
using TeamStuff.mvc.Infrastructure.ViewModels;


namespace TeamStuff.mvc.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<Employee, int> _signInManager;
        public AccountController(SignInManager<Employee, int> signInManager)
        {
            _signInManager = signInManager;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToReturnUrl(returnUrl);

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.Persistent, false);

            if (result == SignInStatus.Success)
                return Redirect("/home/index");

            ModelState.AddModelError("", "Incorrect user name or password");
            return View(model);
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            var autheticationManager = HttpContext.GetOwinContext().Authentication;
            autheticationManager.SignOut();
            return Redirect("login");
        }

        private ActionResult RedirectToReturnUrl(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return Redirect(@"/");
        }
    }
}
