using System;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected ActionResult MessageResult(Message message)
        {
            if (message.ActionResult == ActionStatus.Success)
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        protected ActionResult JsonMessage(object message)
        {
            return new JsonResult
            {
                Data = message,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}