﻿using System.Threading.Tasks;
using System.Web.Mvc;
using TeamStuff.mvc.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize(Roles = "admin"), System.Web.Http.RoutePrefix("api/employee")]
    public class EmployeeController : ApiBaseController
    {
        readonly IEmploeeService _employeService;

        public EmployeeController(IEmploeeService employeService)
        {
            _employeService = employeService;
        }

        [System.Web.Http.HttpGet]
        public JsonResult Get(int id)
        {
            return new JsonResult {Data = _employeService.Get(id), JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("all")]
        public JsonResult GetAll()
        {
            return new JsonResult {Data = _employeService.GetAll(), JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [System.Web.Http.HttpPost]
        public async Task<ActionResult> Create([System.Web.Http.FromBody]EmployeeCreateModel model)
        {
            if (ModelState.IsValid)
                return MessageResult(await _employeService.CreateAsync(model));

            return MessageResult(new ErrorMessage("Model is not valid"));
        }

        [System.Web.Http.HttpPut]
        public async Task<ActionResult> Update([System.Web.Http.FromBody]EmployeeEditModel model)
        {
            if (ModelState.IsValid)
                return MessageResult(await _employeService.UpdateAsync(model));

            return MessageResult(new ErrorMessage("Model is invalid"));
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("remove/{id}")]
        public ActionResult Remove(int id)
        {
            var result = _employeService.Remove(id);
            return MessageResult(new SuccessMessage("Removed"));
        }
    }
}
