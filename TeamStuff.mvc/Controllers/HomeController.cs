﻿using System.Web.Mvc;

namespace TeamStuff.mvc.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
                return Redirect("/admin");

            return Redirect("/user");
        }

        [Authorize(Roles = "admin")]
        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult Employee()
        {
            return View();
        }
    }
}
