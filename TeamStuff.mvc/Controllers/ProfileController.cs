﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using TeamStuff.mvc.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize, System.Web.Http.RoutePrefix("api/profile")]
    public class ProfileController : ApiBaseController
    {
        private readonly IProfileService _service;

        public ProfileController(IProfileService service)
        {
            _service = service;
        }

        [System.Web.Http.HttpGet]
        public ActionResult Profile()
        {
            return new JsonResult
            {
                Data = _service.Get(User.Identity.Name),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("rates")]
        public ActionResult Rates()
        {
            return new JsonResult
            {
                Data = _service.GetRateHistory(User.Identity.Name) ,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("salary")]
        public ActionResult Salary(int year)
        {
            return new JsonResult
            {
                Data = _service.GetSalaryFor(User.Identity.Name, year),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("dayoff")]
        public ActionResult GetDaysOff()
        {
            return new JsonResult
            {
                Data = _service.GetDaysOff(User.Identity.Name),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        [System.Web.Http.HttpPut, System.Web.Http.Route("update-base")]
        public async Task<ActionResult> UpdateBaseInfo([FromBody]ProfileBaseInfoModel model)
        {
            var message = await _service.UpdateBaseInfoAsync(User.Identity.Name, model);
            return MessageResult(message);
        }

        [System.Web.Http.HttpPut, System.Web.Http.Route("update-contact")]
        public async Task<ActionResult> UpdateContactInfo([FromBody]ProfileContactModel model)
        {
            var message = await _service.UpdateContactInfoAsync(User.Identity.Name, model);
            return MessageResult(message);
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("update-password")]
        public async Task<ActionResult> UpdatePassword([FromBody]ChangeAccountModel model)
        {
            var message = await _service.UpdateAccountSettingsAsync(User.Identity.Name, model);
            return MessageResult(message);
        }

        

    }
}
