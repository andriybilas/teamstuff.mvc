using System;
using System.Linq;
using System.Web.Mvc;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using TeamStuff.mvc.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize, System.Web.Http.RoutePrefix("api/report")]
    public class ReportController : ApiBaseController
    {
        private readonly IReportService _service;
        private readonly UserManager<Employee, int> _userManager;

        public ReportController(IReportService service, UserManager<Employee, int> userManager  )
        {
            _service = service;
            _userManager = userManager;
        }

        [System.Web.Http.HttpGet]
        public ActionResult Get(DateTime period)
        {
            return JsonMessage(_service.GetReports(period, User.Identity.Name));
        }

        [System.Web.Http.HttpPost]
        public ActionResult AddReport([System.Web.Http.FromBody] ReportModel model, int? employeeId)
        {
            if (!ModelState.IsValid)
                return MessageResult(new ErrorMessage("Model is not valid"));

            var userName = User.Identity.Name;

            if (employeeId != null)
            {
                var employee = _userManager.Users.FirstOrDefault(u => u.Id == employeeId);

                if (employee == null)
                    return MessageResult(new ErrorMessage("Model is not valid"));

                userName = employee.UserName;
            }

            var message = _service.AddReport(model, userName);
            return MessageResult(message);
        }

        [System.Web.Http.HttpPut]
        public ActionResult UpdateReport([System.Web.Http.FromBody] ReportModel model)
        {
            if (!ModelState.IsValid)
                return MessageResult(new ErrorMessage("Model is not valid"));

            var message = _service.UpdateReport(model);
            return MessageResult(message);
        }

        [System.Web.Http.HttpDelete]
        public ActionResult RemoveReport(int id)
        {
            var message = _service.RemoveReport(id);
            return MessageResult(message);
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("employees"), System.Web.Http.Authorize(Roles = "admin")]
        public ActionResult GetReportUsers(DateTime period)
        {
            return JsonMessage(_service.GetReportUsers(period));
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("detail"), System.Web.Http.Authorize(Roles = "admin")]
        public ActionResult GetEmployeeReportDetails(int employeeId, DateTime period)
        {
            var user = _userManager.Users.FirstOrDefault(u => u.Id == employeeId);

            if (user == null)
                return new JsonResult
                {
                    Data = new ErrorMessage("Expected User doesn't exist"), 
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            return new JsonResult
            {
                Data = _service.GetReports(period, user.UserName),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

    }
}
