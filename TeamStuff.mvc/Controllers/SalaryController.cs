using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TeamStuff.mvc.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize(Roles = "admin"), System.Web.Http.RoutePrefix("api/salary")]
    public class SalaryController : ApiBaseController
    {
        private readonly ISalaryService _salaryService;

        public SalaryController(ISalaryService salaryService)
        {
            _salaryService = salaryService;
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("calculate")]
        public ActionResult CalculateSalary(DateTime salaryPeriod)
        {
            IEnumerable<EvaluateSalaryModel> model = _salaryService.CalculateSalary(salaryPeriod);
            return JsonMessage(model);
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("calculated")]
        public ActionResult CalculatedSalary(DateTime period)
        {
            var model = _salaryService.GetCalculatedSalary(period);
            return JsonMessage(model);
        }

        [System.Web.Http.HttpPost]
        public ActionResult Save([System.Web.Http.FromBody] EvaluateSalaryModel[] model, DateTime period)
        {
            return JsonMessage(_salaryService.SaveCalculation(model, period));
        }
    }
}