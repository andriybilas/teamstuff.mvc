using System.Web.Mvc;
using TeamStuff.mvc.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize(Roles = "admin"), System.Web.Http.RoutePrefix("api/rates")]
    public class RatesController : ApiBaseController
    {
        private readonly ISalaryRateService _salaryService;

        public RatesController(ISalaryRateService salaryService)
        {
            _salaryService = salaryService;
        }

        [System.Web.Http.HttpGet]
        public ActionResult Get(int id)
        {
            return JsonMessage(_salaryService.Get(id));
        }

        [System.Web.Http.HttpPost]
        public ActionResult AddSalaryRate([System.Web.Http.FromBody] SalaryRateModel model)
        {
            if (!ModelState.IsValid)
                return MessageResult(new ErrorMessage("Model is not valid"));

            var message = _salaryService.AddNew(model);
            return MessageResult(message);
        }

        [System.Web.Http.HttpDelete]
        public ActionResult DeleteSalaryRate(int id)
        {
            var message = _salaryService.Remove(id);
            return MessageResult(message);
        }

    }
}