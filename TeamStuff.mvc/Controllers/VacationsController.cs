using System;
using System.Web.Mvc;
using TeamStuff.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Controllers
{
    [System.Web.Http.Authorize(Roles = "admin"), System.Web.Http.RoutePrefix("api/dayoff")]
    public class DayoffController : ApiBaseController
    {
        private readonly IDaysOffService _daysOffService;

        public DayoffController(IDaysOffService daysOffService)
        {
            _daysOffService = daysOffService;
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("all")]
        public ActionResult GetAll()
        {
            return JsonMessage(_daysOffService.GetAll());
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("available")]
        public ActionResult GetAvailablePaidDaysOff(DateTime period, int employeeId)
        {
            var result = _daysOffService.GetAvailablePaidDaysOff(period, employeeId);
            return JsonMessage(result);
        }

        [System.Web.Http.HttpPost]
        public ActionResult SetVacaion([System.Web.Http.FromBody] DayOffModel model)
        {
            if (!ModelState.IsValid)
                return MessageResult(new ErrorMessage("Model is not valid"));

            var message = _daysOffService.AddVacation(model);
            return MessageResult(message);
        }

        [System.Web.Http.HttpDelete]
        public ActionResult RemoveMistaken(int id)
        {
            return MessageResult(_daysOffService.RemoveVacation(id));
        }

    }
}