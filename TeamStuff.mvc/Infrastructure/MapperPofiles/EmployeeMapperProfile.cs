using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.MapperPofiles
{
    public class EmployeeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EmployeeCreateModel, Employee>()
                .ForMember(dest => dest.SelaryRates, opt => opt.ResolveUsing<SalaryRateToEntityResolver>());

            CreateMap<EmployeeEditModel, Employee>();

            CreateMap<Employee, EmployeeEditModel>()
                .ForMember(dest => dest.SalaryRate, opt => opt.ResolveUsing<EntityToSalaryRateModelResolver>())
                .ForMember(dest => dest.SalaryRateTypeName, opt => opt.MapFrom(src => src.SelaryRates.First(s => s.IsCurrent).RateType.ToString()));

            CreateMap<Employee, EmployeeListModel>()
                .ForMember(dest => dest.SalaryRate, opt => opt.ResolveUsing<EntityToSalaryRateModelResolver>());

            CreateMap<Employee, EmployeeSalaryRatesModel>()
                .ForMember(dest => dest.Rates, opt => opt.MapFrom( src => src.SelaryRates));

            CreateMap<SalaryRate, SalaryRateModel>()
                .ForMember(dest => dest.RateType, opt => opt.MapFrom(src => src.RateType.ToString()));

            CreateMap<SalaryRateModel, SalaryRate>();

            CreateMap<Employee, EmployeeDayOffModel>()
                .ForMember(dest => dest.EmployeeName, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(dest => dest.Dismissed, opt => opt.MapFrom(src => src.DismissAt.HasValue))
                .ForMember(dest => dest.VacationUsedDays, opt => opt.ResolveUsing<VacationUsedDaysResolver>())
                .ForMember(dest => dest.VacationAvailableDays, opt => opt.ResolveUsing<VacationAvailableDaysResolver>())
                .ForMember(dest => dest.SicnessUsedDays, opt => opt.ResolveUsing<SicknessUsedDaysResolver>())
                .ForMember(dest => dest.SicnessRemainsPaidDays, opt => opt.ResolveUsing<SicknessRemainsPaidDaysResolver>())
                .ForMember(dest => dest.UsedVacations, opt => opt.ResolveUsing<UsedDaysOffHistory>()
                    .ConstructedBy(()=> new UsedDaysOffHistory(DayOffType.Vacation)))
                .ForMember(dest => dest.UsedSickness, opt => opt.ResolveUsing<UsedDaysOffHistory> ()
                    .ConstructedBy(() => new UsedDaysOffHistory(DayOffType.Sickness)));

            CreateMap<DayOffModel, DayOff>()
                .ForMember(dest => dest.Count, opt => opt.ResolveUsing<VacationCountCalculator>())
                .ForMember(dest => dest.Unpaid, opt => opt.ResolveUsing<VacationUnpaidResolver>());
        }
    }

    public class VacationUnpaidResolver : ValueResolver<DayOffModel, bool>
    {
        int _sicknessDays = 10;
        protected override bool ResolveCore(DayOffModel source)
        {
            if (source.DayOffType == DayOffType.Vacation)
                return source.Unpaid;

            using (var db = new StuffDbContext())
            {
                var employee = db.Users.Include(e => e.Vacations)
                    .FirstOrDefault(e => e.Id == source.EmployeeId);
                
                var period = PeriodHelper.GetSicknessPeriod(employee.HiredAt);
                var usedDays = employee.Vacations.Where(v => 
                    v.DayOffType == DayOffType.Sickness
                    && period.Begin < v.From
                    && period.End > v.From)
                    .Sum(v => (double?) v.Count) ?? 0;

                var sicknesWorkingDays = PeriodHelper.GetWorkingDays(db, source.From, source.To, source.WholeDay);
                return _sicknessDays < usedDays + sicknesWorkingDays;
            }
        }
    }
    public class UsedDaysOffHistory : ValueResolver<Employee, IEnumerable<UsedDayOff>>
    {
        private readonly DayOffType _dayOffType;

        public UsedDaysOffHistory(DayOffType dayOffType)
        {
            _dayOffType = dayOffType;
        }

        protected override IEnumerable<UsedDayOff> ResolveCore(Employee source)
        {
            return source.Vacations.Where(v => v.DayOffType == _dayOffType)
                .Select(v => new UsedDayOff
                {
                    Id = v.Id,
                    From = v.From,
                    To = v.To,
                    Used = v.Count,
                    Unpaid = v.Unpaid
                }).OrderBy(v => v.From).ToList();
        }
    }
    public class VacationCountCalculator : ValueResolver<DayOffModel, double>
    {
        protected override double ResolveCore(DayOffModel source)
        {
            using (var db = new StuffDbContext())
            {
                return PeriodHelper.GetWorkingDays(db, source.From, source.To, source.WholeDay);
            }
        }
    }
    public class VacationAvailableDaysResolver : ValueResolver<Employee, double>
    {
        readonly double _vacDays = 1.5;//double.Parse(Startup.Configuration["Data:VacDays"]);

        protected override double ResolveCore(Employee source)
        {
            var hiredAt = source.HiredAt;
            var workedMonth = (DateTime.Now.Year - hiredAt.Year) * 12 + DateTime.Now.Month - hiredAt.Month;
            return workedMonth * _vacDays - source.Vacations.Where(v => !v.Unpaid && v.DayOffType == DayOffType.Vacation)
                .Sum(v => (double?) v.Count) ?? 0;
        }
    }
    public class SicknessUsedDaysResolver : ValueResolver<Employee, double>
    {
        protected override double ResolveCore(Employee source)
        {
            var period = PeriodHelper.GetSicknessPeriod(source.HiredAt);
            return source.Vacations.Where(v => 
                    v.DayOffType == DayOffType.Sickness
                    && period.Begin < v.From
                    && period.End > v.From)
                .Sum(v => (double?)v.Count) ?? 0;
        }
    }
    public class SicknessRemainsPaidDaysResolver : ValueResolver<Employee, double>
    {
        readonly double _sicknessDays = 10;//double.Parse(Startup.Configuration["Data:VacDays"]);

        protected override double ResolveCore(Employee source)
        {

            var period = PeriodHelper.GetSicknessPeriod(source.HiredAt);
            return _sicknessDays - source.Vacations.Where(v => !v.Unpaid 
                    && v.DayOffType == DayOffType.Sickness
                    && period.Begin < v.From
                    && period.End > v.From )
                .Sum(v => (double?) v.Count) ?? 0;
        }
    }
    public class VacationUsedDaysResolver : ValueResolver<Employee, double>
    {
        protected override double ResolveCore(Employee source)
        {
            return source.Vacations.Where(v => !v.Unpaid && v.DayOffType == DayOffType.Vacation)
                .Sum(v => (double?)v.Count) ?? 0;
        }
    }
    public class EntityToSalaryRateModelResolver : ValueResolver<Employee, int>
    {
        protected override int ResolveCore(Employee source)
        {
            return source.SelaryRates.FirstOrDefault(s => s.IsCurrent)?.Rate ?? 0;
        }
    }
    internal class SalaryRateToEntityResolver : ValueResolver<EmployeeCreateModel, IList<SalaryRate>>
    {
        protected override IList<SalaryRate> ResolveCore(EmployeeCreateModel source)
        {
            var salaryRate = new SalaryRate
            {
                ApplyFrom = DateTime.Now,
                IsCurrent = true,
                Rate = source.SalaryRate,
                RateType = source.SalaryRateType
            };

            return new List<SalaryRate> { salaryRate };
        }
    }

    internal class Period
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
    }
    internal class PeriodHelper
    {
        public static Period GetSicknessPeriod(DateTime hiredAt, DateTime? calculatePeriod = null)
        {
            DateTime period = calculatePeriod ?? DateTime.Now;

            if (hiredAt < period && period < hiredAt.AddMonths(12))
            {
                return new Period { Begin = hiredAt, End = hiredAt.AddMonths(12) };
            }
            return GetSicknessPeriod(hiredAt.AddMonths(12), calculatePeriod);
        }

        public static double GetWorkingDays(StuffDbContext db, DateTime from, DateTime to, bool wholeDay)
        {
            var excludeDates = db.Holidays.Where(h => from >= h.Day && h.Day < to).Select(d => d.Day).ToList();

            var count = 0;
            for (DateTime index = from; index <= to; index = index.AddDays(1))
            {
                if (index.DayOfWeek == DayOfWeek.Saturday || index.DayOfWeek == DayOfWeek.Sunday) continue;
                var excluded = excludeDates.Any(t => index.Date.CompareTo(t.Date) == 0);
                if (!excluded) count++;
            }

            return wholeDay ? count : count - 0.5;
        }
    }
}