using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.MapperPofiles
{
    public class ProfileMapperConfig : Profile
    {
        protected override void Configure()
        {
            CreateMap<Employee, ProfileModel>();
            CreateMap<ProfileBaseInfoModel, Employee>();
            CreateMap<ProfileContactModel, Employee>();

            CreateMap<ReportModel, Report>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<Report, ReportModel>();

            CreateMap<Employee, ReportUser>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(dest => dest.HasReports, opt => opt.MapFrom(src => src.Reports.Any()))
                .ForMember(dest => dest.ReportedHours, opt => opt.MapFrom(src => src.Reports.Sum(r => r.Hours)));
        }
    }
}
