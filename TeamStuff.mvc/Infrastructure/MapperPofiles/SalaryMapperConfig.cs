﻿using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.MapperPofiles
{
    public class SalaryMapperConfig : Profile
    {
        protected override void Configure()
        {
            CreateMap<EvaluateSalaryModel, Salary>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.SalaryRateType, opt => opt.ResolveUsing<SalaryRateModelResolver>())
                .ForMember(dest => dest.Employee, opt => opt.ResolveUsing<EmployeeByIdResolver>());

            CreateMap<Salary, EvaluateSalaryModel > ()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Employee.Id))
                .ForMember(dest => dest.SalaryRateType, opt => opt.MapFrom(src => src.SalaryRateType.ToString()))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => $"{src.Employee.FirstName} {src.Employee.LastName}" ));
        }
    }

    internal class EmployeeByIdResolver : ValueResolver<EvaluateSalaryModel, Employee>
    {
        protected override Employee ResolveCore(EvaluateSalaryModel source)
        {
            using (var db = new StuffDbContext())
            {
                return db.Users.FirstOrDefault(e => e.Id == source.Id);
            }
        }
    }

    internal class SalaryRateModelResolver : ValueResolver<EvaluateSalaryModel, SalaryRateType>
    {
        protected override SalaryRateType ResolveCore(EvaluateSalaryModel source)
        {
            switch (source.SalaryRateType)
            {
                case "Hourly":
                    return SalaryRateType.Hourly;
                case "Monthly":
                    return SalaryRateType.Monthly;
            }

            return SalaryRateType.Monthly;
        }
    }
}