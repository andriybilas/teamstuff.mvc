﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using TeamStuff.mvc.Infrastructure.MapperPofiles;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.Infrastructure.Services
{
    public class DaysOffService : IDaysOffService
    {
        private readonly StuffDbContext _db;
        private readonly IMapper _mapper;

        public DaysOffService(StuffDbContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public IEnumerable<EmployeeDayOffModel> GetAll()
        {
            var employees = _db.Users
                .Include(e => e.Roles)
                .Include(s => s.SelaryRates)
                .Include(e => e.Vacations)
                .Where(e => e.Roles.Count == 0)
                .ToList();

            return _mapper.Map<IEnumerable<EmployeeDayOffModel>>(employees);
        }

        public Message AddVacation(DayOffModel model)
        {
            var vacation = _mapper.Map<DayOff>(model);
            var employee = _db.Users.Include(v => v.Vacations).FirstOrDefault(em => em.Id == model.EmployeeId);

            if (employee == null)
                return new ErrorMessage("Employee is null");

            employee.Vacations.Add(vacation);
            _db.SaveChanges();

            return new SuccessMessage($"Vacation added {vacation.Count}");
        }

        public Message RemoveVacation(int id)
        {
            var vaction = _db.DaysOff.FirstOrDefault(v => v.Id == id);
            if (vaction == null)
                return new ErrorMessage("DayOff is null");

            _db.DaysOff.Remove(vaction);
            _db.SaveChanges();

            return new SuccessMessage("Removed");
        }

        public PaidDaysOffModel GetAvailablePaidDaysOff(DateTime period, int employeeId)
        {
            var result = new PaidDaysOffModel();
            var employee = _db.Users.Include(e => e.Vacations).FirstOrDefault(e => e.Id == employeeId);

            var sicknessPeriod = PeriodHelper.GetSicknessPeriod(employee.HiredAt, period);
            result.PaidSikcness = 10 - employee.Vacations.Where(v => !v.Unpaid
                    && v.DayOffType == DayOffType.Sickness
                    && sicknessPeriod.Begin < v.From
                    && sicknessPeriod.End > v.From)
                .Sum(v => (double?)v.Count) ?? 0;

            var hiredAt = employee.HiredAt;
            var workedMonth = (period.Year - hiredAt.Year) * 12 + period.Month - hiredAt.Month;
            result.PaidVacations = workedMonth * 1.5 - employee.Vacations.Where(v => !v.Unpaid && v.DayOffType == DayOffType.Vacation)
                .Sum(v => (double?)v.Count) ?? 0;

            return result;
        }
    }

    public interface IDaysOffService
    {
        IEnumerable<EmployeeDayOffModel> GetAll();
        Message AddVacation(DayOffModel model);
        Message RemoveVacation(int id);
        PaidDaysOffModel GetAvailablePaidDaysOff(DateTime period, int employeeId);
    }


}
