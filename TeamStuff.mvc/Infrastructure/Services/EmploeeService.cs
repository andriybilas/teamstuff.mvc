﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.Services
{
    public class EmploeeService : SaveService, IEmploeeService
    {
        private readonly StuffDbContext _db;
        private readonly IMapper _mapper;
        private readonly UserManager<Employee, int> _userManager;

        public EmploeeService(StuffDbContext db, IMapper mapper, UserManager<Employee, int> userManager)
        {
            _db = db;
            _mapper = mapper;
            _userManager = userManager;
        }

        public IEnumerable<EmployeeListModel> GetAll()
        {
            var employees = _db.Users
                .Include(e => e.Roles)
                .Include(s => s.SelaryRates)
                .ToList();

            return _mapper.Map<List<EmployeeListModel>>(employees);
        }

        public EmployeeEditModel Get(int id)
        {
            var employee = _db.Users.Include(s => s.SelaryRates).FirstOrDefault(e => e.Id == id);
            return _mapper.Map<EmployeeEditModel>(employee);
        } 
        public Employee Find(string searchKey) => _db.Users.FirstOrDefault(e => e.FirstName.Contains(searchKey) || e.LastName.Contains(searchKey));

        public async Task<Message> CreateAsync(EmployeeCreateModel model)
        {
            try
            {
                var employee = _mapper.Map<Employee>(model);

                foreach (var rate in employee.SelaryRates)
                {
                    _db.SelaryRates.Add(rate);
                }

                _db.SaveChanges();

                var result = await _userManager.CreateAsync(employee, "123");

                if(result.Succeeded)
                    return new SuccessMessage("Employee created");

                return new ErrorMessage(result.Errors);
            }
            catch (Exception ex)
            {
                return new ErrorMessage(ex);
            }
        }

        public async Task<Message> UpdateAsync(EmployeeEditModel model)
        {
            var employee = _userManager.Users.FirstOrDefault(e => e.Id == model.Id);
            _mapper.Map(model, employee);

            try
            {
                var result = await _userManager.UpdateAsync(employee);

                if (result.Succeeded)
                    return new SuccessMessage("Updated");

                return new ErrorMessage(result.Errors);
            }
            catch (Exception ex)
            {
                return new ErrorMessage(ex);
            }

        }

        public Message Remove(int id)
        {
            var employee = _db.Users.FirstOrDefault(e => e.Id == id);
            if (employee == null)
                return new ErrorMessage("Such User doesn't exist");

            _db.Users.Remove(employee);
            return TrySave(_db.SaveChanges);
        }
    }

    public interface IEmploeeService
    {
        IEnumerable<EmployeeListModel> GetAll();
        EmployeeEditModel Get(int id);
        Employee Find(string searchKey);
        Task<Message> CreateAsync(EmployeeCreateModel model);
        Task<Message> UpdateAsync(EmployeeEditModel model);
        Message Remove(int id);
    }
}
