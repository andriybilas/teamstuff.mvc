using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.Services
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<Employee, int> _userManager;
        private readonly StuffDbContext _db;
        private readonly IMapper _mapper;

        public ProfileService(UserManager<Employee, int> userManager, StuffDbContext db, IMapper mapper)
        {
            _userManager = userManager;
            _db = db;
            _mapper = mapper;
        }

        public ProfileModel Get(string identityName)
        {
            var user = _db.Users.FirstOrDefault(e => e.UserName.Equals(identityName));
            return _mapper.Map<ProfileModel>(user);
        }

        public async Task<Message> UpdateBaseInfoAsync(string identityName, ProfileBaseInfoModel model)
        {
            var employee = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(identityName));
            _mapper.Map(model, employee);

            var result = await _userManager.UpdateAsync(employee);

            if (result.Succeeded)
                return new SuccessMessage("");
            return new ErrorMessage(result.Errors);
        }

        public async Task<Message> UpdateContactInfoAsync(string identityName, ProfileContactModel model)
        {
            try
            {
                var employee = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(identityName));
                _mapper.Map(model, employee);

                var result = await _userManager.UpdateAsync(employee);

                if (result.Succeeded)
                    return new SuccessMessage("");
            }
            catch (Exception ex)
            {
                return new ErrorMessage(ex);
            }

            return new ErrorMessage("Something wrong. Talk to Andriy.");
        }

        public IEnumerable<SalaryRateModel> GetRateHistory(string identityName)
        {
            var employee = _userManager.Users.Include(u => u.SelaryRates)
                .FirstOrDefault(u => u.UserName.Equals(identityName));

            return _mapper.Map<IEnumerable<SalaryRateModel>>(employee.SelaryRates);
        }

        public IEnumerable<EvaluateSalaryModel> GetSalaryFor(string identityName, int year)
        {
            var salaries = _userManager.Users.Include(e => e.Salaries)
                .FirstOrDefault(u => u.UserName.Equals(identityName))
                .Salaries.Where(s => s.Period >= new DateTime(year, 01,01) && s.Period < new DateTime(year, 12, 31) );

            if (!salaries.Any())
                return Enumerable.Empty<EvaluateSalaryModel>();

            return _mapper.Map<IEnumerable<EvaluateSalaryModel>>(salaries);
        }

        public EmployeeDayOffModel GetDaysOff(string identityName)
        {
            var employee = _userManager.Users
                .Include(s => s.SelaryRates)
                .Include(e => e.Vacations)
                .FirstOrDefault(e => e.UserName.Equals(identityName));

            return _mapper.Map<EmployeeDayOffModel>(employee);
        }

        public async Task<Message> UpdateAccountSettingsAsync(string identityName, ChangeAccountModel model)
        {
            var employee = _userManager.Users
                .FirstOrDefault(e => e.UserName.Equals(identityName));

            if (employee == null)
                return new ErrorMessage("Such Employee doesn't exist");

            try
            {
                employee.UserName = model.UserName;
                
                var result = await _userManager.ChangePasswordAsync(employee.Id, model.OldPassword, model.NewPassword);

                if (!result.Succeeded)
                    return new ErrorMessage(result.Errors);

                var updateResult = await _userManager.UpdateAsync(employee);
                if (!updateResult.Succeeded)
                    return new ErrorMessage(updateResult.Errors);

                return new ErrorMessage(result.Errors);
            }
            catch (Exception ex)
            {
                return new ErrorMessage(ex);
            }
        }
    }

    public interface IProfileService
    {
        ProfileModel Get(string identityName);
        Task<Message> UpdateBaseInfoAsync(string identityName, ProfileBaseInfoModel model);
        Task<Message> UpdateContactInfoAsync(string identityName, ProfileContactModel model);
        IEnumerable<SalaryRateModel> GetRateHistory(string identityName);
        IEnumerable<EvaluateSalaryModel> GetSalaryFor(string identityName, int year);
        EmployeeDayOffModel GetDaysOff(string identityName);
        Task<Message> UpdateAccountSettingsAsync(string name, ChangeAccountModel model);
    }
}
