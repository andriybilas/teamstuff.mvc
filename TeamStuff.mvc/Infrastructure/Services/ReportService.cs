using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using TeamStuff.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.Services
{
    public class ReportService : SaveService, IReportService
    {
        private readonly UserManager<Employee, int> _userManager;
        private readonly IMapper _mapper;
        private readonly StuffDbContext _db;

        public ReportService(UserManager<Employee, int> userManager, IMapper mapper, StuffDbContext db )
        {
            _userManager = userManager;
            _mapper = mapper;
            _db = db;
        }

        public IEnumerable<ReportModel> GetReports(DateTime period, string identityName)
        {
            var periodFrom = period;
            var periodTo = period.AddMonths(1).AddDays(-1);
            var reports = _userManager.Users
                .Include(r => r.Reports)
                .FirstOrDefault(e => e.UserName.Equals(identityName))
                .Reports.Where(r => r.Date >= periodFrom && r.Date <= periodTo)
                .OrderBy(r => r.Date);

            if (!reports.Any())
                return Enumerable.Empty<ReportModel>();

            return _mapper.Map<IEnumerable<ReportModel>>(reports);
        }

        public Message AddReport(ReportModel model, string identityName)
        {
            var employee = _userManager.Users.Include(r => r.Reports)
                .FirstOrDefault(e => e.UserName.Equals(identityName));

            var report = _mapper.Map<Report>(model);
            employee.Reports.Add(report);
            return TrySave(_db.SaveChanges);
        }

        public Message RemoveReport(int id)
        {
            var report = _db.Reports.FirstOrDefault(r => r.Id == id);
            if (report != null) _db.Reports.Remove(report);
            return TrySave(_db.SaveChanges);
        }

        public Message UpdateReport(ReportModel model)
        {
            var report = _db.Reports.FirstOrDefault(r => r.Id == model.Id);
            _mapper.Map(model, report);
            return TrySave(_db.SaveChanges);
        }

        public IEnumerable<ReportUser> GetReportUsers(DateTime period)
        {
            var periodFrom = period;
            var periodTo = period.AddMonths(1).AddDays(-1);

            var employees = _db.Users.LeftJoin(_db.Reports,
                e => e.Id,
                r => r.EmployeeId,
                (emp, report) => new {
                    Id = emp.Id,
                    FullName = $"{emp.FirstName} {emp.LastName}",
                    Report = report
                })
                .Where(rep => rep.Report == null || rep.Report.Date >= periodFrom && rep.Report.Date <= periodTo)
                .GroupBy(em => new { em.Id, em.FullName } )
                .Select(emp => new ReportUser
                {
                    Id = emp.Key.Id,
                    FullName = emp.Key.FullName,
                    HasReports = emp.Sum(r => (int?)r.Report?.Hours ?? 0) != 0,
                    ReportedHours = emp.Sum(r => (int?)r.Report?.Hours ?? 0)
                });
                
            return employees;
        }
    }

    public interface IReportService
    {
        IEnumerable<ReportModel> GetReports(DateTime period, string identityName);
        Message AddReport(ReportModel report, string identityName);
        Message RemoveReport(int id);
        Message UpdateReport(ReportModel model);
        IEnumerable<ReportUser> GetReportUsers(DateTime period);
    }

    public static class LinqExtended
    {
        public static IEnumerable<TResult> LeftJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source,
                                                 IEnumerable<TInner> inner,
                                                 Func<TSource, TKey> pk,
                                                 Func<TInner, TKey> fk,
                                                 Func<TSource, TInner, TResult> result)
        {
            IEnumerable<TResult> _result = Enumerable.Empty<TResult>();

            _result = from s in source
                      join i in inner
                      on pk(s) equals fk(i) into joinData
                      from left in joinData.DefaultIfEmpty()
                      select result(s, left);

            return _result;
        }
    }
}
