﻿using System.Data.Entity;
using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using Microsoft.AspNet.Identity;
using TeamStuff.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;
using WebGrease.Css.Extensions;

namespace TeamStuff.mvc.Infrastructure.Services
{
    internal class SalaryRateService : SaveService, ISalaryRateService
    {
        private readonly StuffDbContext _db;
        private readonly IMapper _mapper;
        private readonly UserManager<Employee, int> _userManager;

        public SalaryRateService(StuffDbContext db, IMapper mapper, UserManager<Employee, int> userManager)
        {
            _db = db;
            _mapper = mapper;
            _userManager = userManager;
        }

        public EmployeeSalaryRatesModel Get(int id)
        {
            var model = _db.Users.Include(e => e.SelaryRates).FirstOrDefault(em => em.Id == id);
            return _mapper.Map<EmployeeSalaryRatesModel>(model);
        }

        public Message AddNew(SalaryRateModel model)
        {
            var salaryRate = _mapper.Map<SalaryRate>(model);
            var employee = _userManager.Users.Include(e => e.SelaryRates).FirstOrDefault(u => u.Id == model.EmployeeId);

            if (employee == null)
                return new ErrorMessage("Employee is null.");

            if (model.IsCurrent && employee.SelaryRates.Any())
                employee.SelaryRates.ForEach(r => r.IsCurrent = false);

            salaryRate.EmployeeId = employee.Id;
            _db.SelaryRates.Add(salaryRate);

            return TrySave(_db.SaveChanges);
        }

        public Message Remove(int id)
        {
            var rate = _db.SelaryRates.Include(e => e.Employee).FirstOrDefault(s => s.Id == id);
            var userRates = _db.SelaryRates.Include(e => e.Employee).Where(s => s.Employee.Id == rate.Employee.Id);

            if (userRates.Count() == 1)
                return new ErrorMessage("Can't remove last salary rate. Create new and then remove this one.");

            if (rate == null)
                return new ErrorMessage("Salary rate is null.");

            _db.SelaryRates.Remove(rate);
            //_db.SaveChanges();

            if (rate.IsCurrent)
            {
                var last = _db.SelaryRates.Where(s => s.Employee.Id == rate.EmployeeId).OrderByDescending(ur => ur.ApplyFrom).First();
                last.IsCurrent = true;
            }
            
            return TrySave(_db.SaveChanges);
        }
    }

    public interface ISalaryRateService
    {
        EmployeeSalaryRatesModel Get(int id);
        Message AddNew(SalaryRateModel model);
        Message Remove(int id);
    }

}
