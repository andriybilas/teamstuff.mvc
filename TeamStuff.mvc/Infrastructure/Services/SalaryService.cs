﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using DataContext.ef.Models;
using TeamStuff.Infrastructure.Services;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.Services
{
    internal class DayOffEmployee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DayOff DayOff { get; set; }
    }

    public class SalaryService : SaveService, ISalaryService
    {
        private readonly StuffDbContext _db;
        private readonly IMapper _mapper;

        public SalaryService(StuffDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public IEnumerable<EvaluateSalaryModel> CalculateSalary(DateTime salaryPeriod)
        {
            var periodFrom = new DateTime(salaryPeriod.Year, salaryPeriod.Month, 1, 12, 0, 0);
            var periodTo = periodFrom.AddMonths(1).AddDays(-1);
            var employees = _db.Users
                .Include(e => e.Roles)
                .Where(em => em.Roles.Count == 0 && 
                !em.DismissAt.HasValue || (em.DismissAt.HasValue && em.DismissAt > periodFrom)).ToList();

            return employees.Select(employee => CalculateSalary(employee, periodFrom, periodTo)).Where(e => e.WorkingDays > 0);
        }

        public Message SaveCalculation(IEnumerable<EvaluateSalaryModel> model, DateTime period)
        {
            var calendarPeriod = new DateTime(period.Year, period.Month, 1, 12, 0, 0);
            var previosCalculation = _db.Salaries.Where(s => s.Period == calendarPeriod && s.CalculationType == CalculationType.Complete);

            if (previosCalculation.Any())
                _db.Salaries.RemoveRange(previosCalculation);

            var salaryCalculations = _mapper.Map<IEnumerable<Salary>>(model).ToList();
            salaryCalculations.ForEach(calc => calc.Period = calendarPeriod);
            _db.Salaries.AddRange(salaryCalculations);
            return TrySave(_db.SaveChanges);
        }

        public IEnumerable<EvaluateSalaryModel> GetCalculatedSalary(DateTime period)
        {
            var calendarPeriod = new DateTime(period.Year, period.Month, 1, 12, 0, 0);
            var savedSalary = _db.Salaries.Include(s => s.Employee).Where(s => s.Period == calendarPeriod && s.CalculationType == CalculationType.Complete).ToList();

            if (savedSalary.Count == 0)
                return Enumerable.Empty<EvaluateSalaryModel>();

            return _mapper.Map<IEnumerable<EvaluateSalaryModel>>(savedSalary);
        }


        private EvaluateSalaryModel CalculateSalary(Employee employee, DateTime from, DateTime to)
        {
            var calendarPeriod = new DateTime(from.Year, from.Month, 1, 12, 0, 0);
            var interimResult = _db.Salaries.Include(s => s.Employee)
                    .FirstOrDefault( s => s.Period == calendarPeriod && 
                            s.Employee.Id == employee.Id &&
                            s.CalculationType == CalculationType.Interim);

            var currentRate = _db.SelaryRates.Where(sr => sr.Employee.Id == employee.Id && sr.ApplyFrom <= from)
                .OrderByDescending(sr => sr.ApplyFrom).FirstOrDefault() ?? 
                    _db.SelaryRates.FirstOrDefault(sr => sr.Employee.Id == employee.Id && sr.IsCurrent);

            int workingDays = GetWorkingDays(from, to);
            double employeeWorkingDays = GetWorkingDays(employee.HiredAt, employee.DismissAt, from, to);

            double paydSickness = _db.DaysOff
                .Where(d => d.Employee.Id == employee.Id && d.From >= from && d.To <= to && d.DayOffType == DayOffType.Sickness && !d.Unpaid)
                    .Sum(day => (double?)day.Count) ?? 0;

            double unPaydSickness = _db.DaysOff
                .Where(d => d.Employee.Id == employee.Id && d.From >= from && d.To <= to && d.DayOffType == DayOffType.Sickness && d.Unpaid)
                    .Sum(day => (double?)day.Count) ?? 0;

            double paydVacations = _db.DaysOff
                .Where(d => d.Employee.Id == employee.Id && d.From >= from && d.To <= to && d.DayOffType == DayOffType.Vacation && !d.Unpaid)
                    .Sum(day => (double?)day.Count) ?? 0;

            double unPaydVacations = _db.DaysOff
                .Where(d => d.Employee.Id == employee.Id && d.From >= from && d.To <= to && d.DayOffType == DayOffType.Vacation && d.Unpaid)
                    .Sum(day => (double?)day.Count) ?? 0;

            employeeWorkingDays = employeeWorkingDays - paydSickness - unPaydSickness - paydVacations - unPaydVacations;


            if (currentRate.RateType == SalaryRateType.Hourly)
            {
                return new EvaluateSalaryModel
                {
                    Id = employee.Id,
                    ReportedHours = interimResult?.ReportedHours ?? 0,
                    Name = $"{employee.FirstName} {employee.LastName}",
                    WorkingDays = employeeWorkingDays,
                    WorkingSum = (int) Math.Round(employeeWorkingDays * currentRate.Rate.Value),
                    SicknessDays = paydSickness,
                    SicknessDaysUnpaid = unPaydSickness,
                    VacationDays = paydVacations,
                    VacationDaysUnpaid = unPaydVacations,
                    SicknessSum = (int) Math.Round(paydSickness * 8 * currentRate.Rate.Value),
                    VacationSum = (int) Math.Round(paydVacations * 8 * currentRate.Rate.Value),
                    Advance = interimResult?.Advance ?? 0,
                    OtherGrants = interimResult?.OtherGrants ?? 0,
                    SalaryRateType = SalaryRateType.Hourly.ToString(),
                    Rate = currentRate.Rate.Value,
                    Period = calendarPeriod
                };
            }

            double daylyRate = (double)currentRate.Rate.Value / workingDays;

            return new EvaluateSalaryModel
            {
                Id = employee.Id,
                Name = $"{employee.FirstName} {employee.LastName}",
                WorkingDays = employeeWorkingDays,
                WorkingSum = (int) Math.Round(employeeWorkingDays * daylyRate),
                SicknessDays = paydSickness,
                SicknessDaysUnpaid = unPaydSickness,
                VacationDays = paydVacations,
                VacationDaysUnpaid = unPaydVacations,
                Advance = interimResult?.Advance ?? 0,
                OtherGrants = interimResult?.OtherGrants ?? 0,
                SalaryRateType = SalaryRateType.Monthly.ToString(),
                SicknessSum = (int) Math.Round(paydSickness * daylyRate),
                VacationSum = (int) Math.Round(paydVacations * daylyRate),
                Rate = currentRate.Rate.Value,
                Period = calendarPeriod
            };
        }

        private int GetWorkingDays(DateTime from, DateTime to)
        {
            var excludeDates = _db.Holidays.Where(h => from <= h.Day && h.Day <= to).Select(d => d.Day);
            var count = 0;

            for (DateTime index = from; index <= to; index = index.AddDays(1))
            {
                if (index.DayOfWeek == DayOfWeek.Saturday || index.DayOfWeek == DayOfWeek.Sunday) continue;
                var excluded = excludeDates.ToList().Any(t => index.Date.CompareTo(t.Date) == 0);
                if (!excluded) count++;
            }

            return count;
        }

        protected int GetWorkingDays(DateTime hiredAt, DateTime? dissmisedAt, DateTime from, DateTime to)
        {
            if (hiredAt > from)
                from = hiredAt;

            if (dissmisedAt.HasValue && dissmisedAt < to)
                to = dissmisedAt.Value;

            return GetWorkingDays(from, to);
        }
    }

    public interface ISalaryService
    {
        IEnumerable<EvaluateSalaryModel> CalculateSalary(DateTime salaryPeriod);
        Message SaveCalculation(IEnumerable<EvaluateSalaryModel> model, DateTime period);
        IEnumerable<EvaluateSalaryModel> GetCalculatedSalary(DateTime period);
    }

}
