﻿using System;
using TeamStuff.mvc.Infrastructure.ViewModels;

namespace TeamStuff.mvc.Infrastructure.Services
{
    public class SaveService
    {
        protected Message TrySave(Func<int> action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                return new ErrorMessage($"Type: {ex.GetType()} Error: {ex.Message}");
            }
            return new SuccessMessage();
        }
    }
}