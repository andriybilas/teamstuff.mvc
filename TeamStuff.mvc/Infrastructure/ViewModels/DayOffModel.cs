﻿using System;
using DataContext.ef.Models;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class DayOffModel
    {
        public int EmployeeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool WholeDay { get; set; }
        public bool Unpaid { get; set; }
        public DayOffType DayOffType { get; set; }

    }
}
