﻿using System;
using System.Collections.Generic;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class UsedDayOff
    {
        public int Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public double Used { get; set; }
        public bool Unpaid { get; set; }
    }

    public class EmployeeDayOffModel
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public bool Dismissed { get; set; }
        public IEnumerable<UsedDayOff> UsedVacations { get; set; }
        public IEnumerable<UsedDayOff> UsedSickness { get; set; }
        public double VacationUsedDays { get; set; }
        public double VacationAvailableDays { get; set; }
        public double SicnessUsedDays { get; set; }
        public double SicnessRemainsPaidDays { get; set; }
    }
}
