﻿using System;
using System.Collections.Generic;
using DataContext.ef.Models;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string MobilePhone { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime HiredAt { get; set; }
        public string JobPosition { get; set; }
        public DateTime? DismissAt { get; set; }
    }

    public class EmployeeCreateModel : EmployeeModel
    {
        public int SalaryRate { get; set; }
        public SalaryRateType SalaryRateType { get; set; }
    }

    public class EmployeeListModel : EmployeeCreateModel { }

    public class EmployeeEditModel : EmployeeModel
    {
        public int SalaryRate { get; set; }
        public string SalaryRateTypeName { get; set; }
    }

    public class EmployeeSalaryRatesModel : EmployeeModel
    {
        public IEnumerable<SalaryRateModel> Rates { get; set; }
    }

    public class SalaryRateModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime ApplyFrom { get; set; }
        public string RateType { get; set; }
        public int? Rate { get; set; }
        public bool IsCurrent { get; set; }
    }
}
