﻿using System;
using DataContext.ef.Models;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class EvaluateSalaryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SalaryRateType { get; set; }
        public int Rate { get; set; }
        public DateTime Period { get; set; }
        public CalculationType CalculationType { get; set; }

        public double WorkingDays { get; set; }
        public double ReportedHours { get; set; }
        public int WorkingSum { get; set; }

        public double VacationDays { get; set; }
        public int VacationSum { get; set; }

        public double SicknessDays { get; set; }
        public int SicknessSum { get; set; }

        public int Advance { get; set; }
        public int OtherGrants { get; set; }
       
        public int EvaluatedSalary { get; set; }
        public double SicknessDaysUnpaid { get; set; }
        public double VacationDaysUnpaid { get; set; }
    }
}
