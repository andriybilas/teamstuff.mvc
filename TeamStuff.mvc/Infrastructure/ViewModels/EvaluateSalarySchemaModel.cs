﻿using System;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class EvaluateSalarySchemaModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Advance { get; set; }
        public int ReportedHours { get; set; }
        public string SalaryRateType { get; set; }
        public DateTime HiredAt { get; set; }
        public DateTime? DismissAt { get; set; }
        public int OtherGrants { get; set; }
    }
}
