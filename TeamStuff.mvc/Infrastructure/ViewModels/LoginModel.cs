﻿using System.ComponentModel.DataAnnotations;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public bool Persistent { get; set; }
    }
}
