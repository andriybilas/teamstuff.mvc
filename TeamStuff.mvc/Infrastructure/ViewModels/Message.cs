﻿using System;
using System.Collections.Generic;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public enum ActionStatus
    {
        Success, 
        Error
    }
    public abstract class Message
    {
        public virtual ActionStatus ActionResult { get; set; }
        public virtual string MessageText { get; set; }
    }

    public sealed class ErrorMessage : Message
    {
        public ErrorMessage(string message)
        {
            MessageText = message;
        }

        public ErrorMessage(Exception exception)
        {
            var message = exception.Message;
            if (exception.InnerException != null)
                message += $"Inner exception: {exception.InnerException.Message}";

            MessageText = message;
        }

        public ErrorMessage(IEnumerable<string> errors)
        {
            foreach (var error in errors)
            {
                MessageText += $"[{error}]";
            }
        }

        public override ActionStatus ActionResult => ActionStatus.Error;
    }

    public sealed class SuccessMessage : Message
    {
        public SuccessMessage()
        {
        }

        public SuccessMessage(string message)
        {
            MessageText = message;
        }

        public override ActionStatus ActionResult => ActionStatus.Success;
    }
}