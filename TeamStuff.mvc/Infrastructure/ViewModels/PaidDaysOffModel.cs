namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class PaidDaysOffModel
    {
        public double PaidVacations { get; set; }
        public double PaidSikcness { get; set; }
    }
}
