using System;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class ProfileBaseInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
    }

    public class ProfileContactModel
    {
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string Skype { get; set; }
    }
}
