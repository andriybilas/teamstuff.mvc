using System;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class ProfileModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public string MobilePhone { get; set; }
        public DateTime HiredAt { get; set; }
        public DateTime? DismissAt { get; set; }
        public string Skype { get; set; }
    }

    public class ChangeAccountModel
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

    }
}
