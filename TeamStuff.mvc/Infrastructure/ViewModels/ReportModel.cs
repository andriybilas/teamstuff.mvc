using System;
using System.ComponentModel.DataAnnotations;

namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class ReportModel
    {
        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Hours { get; set; }
    }
}
