namespace TeamStuff.mvc.Infrastructure.ViewModels
{
    public class ReportUser
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public bool HasReports { get; set; }
        public double ReportedHours { get; set; }
    }
}
