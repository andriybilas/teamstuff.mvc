﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('api', ['$http', '$q', 'toaster',
    function ($http, $q, toaster) {

            var getData = function (url, params) {
                var deferred = $q.defer();
                $http.get(url, {
                    params: params,
                    headers: {
                        contentType: 'application/json'
                    }
                }).success(function (result, status, resHeaders) {
                    deferred.resolve(result.data);
                }).error(function (error, status) {
                    toaster.pop('error', 'Serve error', error);
                    deferred.reject({ error: error, status: status });
                });
                return deferred.promise;
            };

            var postData = function (url, params) {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: url,
                    data: params
                }).success(function (data) {
                    return deferred.resolve(data);
                }).error(function (error, status) {
                    toaster.pop('error', 'Serve error', error);
                    deferred.reject({ error: error, status: status });
                });
                return deferred.promise;
            };

            var putData = function (url, params) {
                var deferred = $q.defer();

                $http({
                    method: 'PUT',
                    url: url,
                    data: params
                }).success(function (data) {
                    return deferred.resolve(data);
                }).error(function (error, status) {
                    toaster.pop('error', 'Serve error', error);
                    deferred.reject({ error: error, status: status });
                });
                return deferred.promise;
            };

            var deleteData = function (url, params) {
                var deferred = $q.defer();
                $http.delete(url, {
                    params: params
                })
                .success(function (data) {
                    return deferred.resolve(data);
                })
                .error(function (error, status) {
                    toaster.pop('error', 'Serve error', error);
                    deferred.reject({ error: error, status: status });
                });
                return deferred.promise;
            };

            return {
                get: getData,
                post: postData,
                put: putData,
                remove: deleteData
            };
        }]);
})()