(function() {
    'use strict';

    angular.module('TeamStuff')
        // =========================================================================
        // Base controller for common functions
        // =========================================================================
        .controller('mainCtrl', function($timeout, $state, $scope, growlService) {


            // Detact Mobile Browser
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                angular.element('html').addClass('ismobile');
            }

            // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
            this.sidebarToggle = {
                left: true,
                right: false
            }

            // By default template has a boxed layout
            this.layoutType = localStorage.getItem('ma-layout-status');

            // For Mainmenu Active Class
            this.$state = $state;

            //Close sidebar on click
            this.sidebarStat = function(event) {
                if (!angular.element(event.target).parent().hasClass('active')) {
                    //this.sidebarToggle.left = false;
                }
            }

            //Listview Search (Check listview pages)
            this.listviewSearchStat = false;

            this.lvSearch = function() {
                this.listviewSearchStat = true;
            }

            //Listview menu toggle in small screens
            this.lvMenuStat = false;

            //Blog
            this.wallCommenting = [];

            this.wallImage = false;
            this.wallVideo = false;
            this.wallLink = false;

            //Skin Switch
            this.currentSkin = 'orange';

        })


        // =========================================================================
        // ANIMATIONS DEMO
        // =========================================================================
        .controller('animCtrl', function($timeout) {

            //Animation List
            this.attentionSeekers = [
                { animation: 'bounce', target: 'attentionSeeker' },
                { animation: 'flash', target: 'attentionSeeker' },
                { animation: 'pulse', target: 'attentionSeeker' },
                { animation: 'rubberBand', target: 'attentionSeeker' },
                { animation: 'shake', target: 'attentionSeeker' },
                { animation: 'swing', target: 'attentionSeeker' },
                { animation: 'tada', target: 'attentionSeeker' },
                { animation: 'wobble', target: 'attentionSeeker' }
            ];
            this.flippers = [
                { animation: 'flip', target: 'flippers' },
                { animation: 'flipInX', target: 'flippers' },
                { animation: 'flipInY', target: 'flippers' },
                { animation: 'flipOutX', target: 'flippers' },
                { animation: 'flipOutY', target: 'flippers' }
            ];
            this.lightSpeed = [
                { animation: 'lightSpeedIn', target: 'lightSpeed' },
                { animation: 'lightSpeedOut', target: 'lightSpeed' }
            ];
            this.special = [
                { animation: 'hinge', target: 'special' },
                { animation: 'rollIn', target: 'special' },
                { animation: 'rollOut', target: 'special' }
            ];
            this.bouncingEntrance = [
                { animation: 'bounceIn', target: 'bouncingEntrance' },
                { animation: 'bounceInDown', target: 'bouncingEntrance' },
                { animation: 'bounceInLeft', target: 'bouncingEntrance' },
                { animation: 'bounceInRight', target: 'bouncingEntrance' },
                { animation: 'bounceInUp', target: 'bouncingEntrance' }
            ];
            this.bouncingExits = [
                { animation: 'bounceOut', target: 'bouncingExits' },
                { animation: 'bounceOutDown', target: 'bouncingExits' },
                { animation: 'bounceOutLeft', target: 'bouncingExits' },
                { animation: 'bounceOutRight', target: 'bouncingExits' },
                { animation: 'bounceOutUp', target: 'bouncingExits' }
            ];
            this.rotatingEntrances = [
                { animation: 'rotateIn', target: 'rotatingEntrances' },
                { animation: 'rotateInDownLeft', target: 'rotatingEntrances' },
                { animation: 'rotateInDownRight', target: 'rotatingEntrances' },
                { animation: 'rotateInUpLeft', target: 'rotatingEntrances' },
                { animation: 'rotateInUpRight', target: 'rotatingEntrances' }
            ];
            this.rotatingExits = [
                { animation: 'rotateOut', target: 'rotatingExits' },
                { animation: 'rotateOutDownLeft', target: 'rotatingExits' },
                { animation: 'rotateOutDownRight', target: 'rotatingExits' },
                { animation: 'rotateOutUpLeft', target: 'rotatingExits' },
                { animation: 'rotateOutUpRight', target: 'rotatingExits' }
            ];
            this.fadeingEntrances = [
                { animation: 'fadeIn', target: 'fadeingEntrances' },
                { animation: 'fadeInDown', target: 'fadeingEntrances' },
                { animation: 'fadeInDownBig', target: 'fadeingEntrances' },
                { animation: 'fadeInLeft', target: 'fadeingEntrances' },
                { animation: 'fadeInLeftBig', target: 'fadeingEntrances' },
                { animation: 'fadeInRight', target: 'fadeingEntrances' },
                { animation: 'fadeInRightBig', target: 'fadeingEntrances' },
                { animation: 'fadeInUp', target: 'fadeingEntrances' },
                { animation: 'fadeInBig', target: 'fadeingEntrances' }
            ];
            this.fadeingExits = [
                { animation: 'fadeOut', target: 'fadeingExits' },
                { animation: 'fadeOutDown', target: 'fadeingExits' },
                { animation: 'fadeOutDownBig', target: 'fadeingExits' },
                { animation: 'fadeOutLeft', target: 'fadeingExits' },
                { animation: 'fadeOutLeftBig', target: 'fadeingExits' },
                { animation: 'fadeOutRight', target: 'fadeingExits' },
                { animation: 'fadeOutRightBig', target: 'fadeingExits' },
                { animation: 'fadeOutUp', target: 'fadeingExits' },
                { animation: 'fadeOutUpBig', target: 'fadeingExits' }
            ];
            this.zoomEntrances = [
                { animation: 'zoomIn', target: 'zoomEntrances' },
                { animation: 'zoomInDown', target: 'zoomEntrances' },
                { animation: 'zoomInLeft', target: 'zoomEntrances' },
                { animation: 'zoomInRight', target: 'zoomEntrances' },
                { animation: 'zoomInUp', target: 'zoomEntrances' }
            ];
            this.zoomExits = [
                { animation: 'zoomOut', target: 'zoomExits' },
                { animation: 'zoomOutDown', target: 'zoomExits' },
                { animation: 'zoomOutLeft', target: 'zoomExits' },
                { animation: 'zoomOutRight', target: 'zoomExits' },
                { animation: 'zoomOutUp', target: 'zoomExits' }
            ];

            //Animate    
            this.ca = '';

            this.setAnimation = function(animation, target) {
                if (animation === "hinge") {
                    animationDuration = 2100;
                } else {
                    animationDuration = 1200;
                }

                angular.element('#' + target).addClass(animation);

                $timeout(function() {
                    angular.element('#' + target).removeClass(animation);
                }, animationDuration);
            }

        })
        .controller('loginCtrl', function () {
            var vm = this;
            vm.login = 1;
            vm.register = 0;
            vm.forgot = 0;

            vm.submitForm = function ($event, valid) {
                vm.submitted = true;

                if (!valid) {
                    $event.preventDefault();
                    return false;
                }
                    

            }
        })
        .controller('headerCtrl', function ($timeout, messageService) {

        });
})();