﻿(function() {
    'use strict';

    angular.module('TeamStuff')
        .directive('bpDropdown', ['underscore', '$timeout', '$document', function (_, $timeout, $document) {
            return {
                restrict: 'A',
                transclude: false,
                templateUrl: 'Scripts/app/directives/bp-dropdown.html',
                scope: {
                    collection: '=',
                    changed: '=',
                    selected: '=',
                    iconClass: '@',
                    headerText: '@',
                    itemValue: '@',
                    itemText: '@'
                },
                link: function (scope, $element) {
                    var that = scope;
                    var classOff = 'ion-android-radio-button-off';
                    var classOn = 'ion-android-radio-button-on';

                    that.onChange = function (event, item) {
                        event.stopPropagation();
                        event.preventDefault();

                        var $menu = angular.element(event.currentTarget).parent();
                        if ($menu.hasClass('show'))
                            $menu.removeClass('show').addClass('hide');

                        _.each($menu.find('i'), function (icon) {
                            angular.element(icon).removeClass(classOn).addClass(classOff);
                        });

                        var $icon = angular.element(event.currentTarget).find('i');
                        $icon.removeClass(classOff).addClass(classOn);

                        var $text = $menu.parent().find('button').find('span');
                        $text.text(this.item[that.itemText]);

                        if (!!that.selected && that.selected[that.itemValue] === item[that.itemValue])
                            return;

                        if (!!that.selected) {
                            that.selected = item;
                        }
                            
                        $timeout(function () {
                            if (!!that.changed)
                                that.changed(item);
                        });
                    };

                    that.onMenuOpen = function (event) {
                        event.stopPropagation();
                        event.preventDefault();

                        if ($element.find('ul').hasClass('hide')) {
                            $element.find('ul').removeClass('hide').addClass('show');
                        } else {
                            $element.find('ul').removeClass('show').addClass('hide');
                        }
                    }

                    that.isSelected = function(item) {
                        if (!!that.selected && !!that.selected[that.itemValue] && item[that.itemValue] === that.selected[that.itemValue]) {
                            return classOn;
                        }
                        return classOff;
                    };

                    scope.$watch('selected', function (selected) {
                        if (!!selected) {
                            var $text = $element.find('button').find('span');
                            $text.text(selected[that.itemText]);
                        }
                    });

                    $document.on('click', function (event) {
                        if ($element.find('ul').hasClass('show')) {
                            $element.find('ul').removeClass('show').addClass('hide');
                        }
                    });
                }
            }
        }]);
})()