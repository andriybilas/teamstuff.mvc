(function() {
    'use strict';

    angular.module('TeamStuff')
    .directive('dateWrapper', function () {
        return {
            restrict: 'A',
            controller: 'dateWrapperCtrl',
            controllerAs: 'dctrl',
            templateUrl: 'Scripts/app/directives/date-wrapper.html',
            bindToController: {
                model: '=ngModel',
                name: '@',
                required: '@',
                color: '@',
                datepickerOptions: '=',
                format: '@',
                placeholder:'@'
            },
            scope: true
        };
    })
    .controller('dateWrapperCtrl', ['$scope', function ($scope) {
        var dctrl = this;
        dctrl.dateformat = dctrl.format || 'yyyy-MM-dd';

        dctrl.options = {
            opened: false,
            today: function () {
                dctrl.model = new Date();
            },
            clear: function () {
                dctrl.model = null;
            },
            open: function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                dctrl.options.opened = true;
            }
        };

        dctrl.options.dateOptions = dctrl.datepickerOptions || {
            formatYear: 'yy',
            startingDay: 1,
        };

    }]);
})()