﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .filter('mobile', ['$filter', function ($filter) {
        return function (input) {
            if (!input) {
                return '';
            }

            return '+38 (0' + input[0] + input[1] + ') ' + input[2] + input[3] + input[4] + ' ' + input[5] + input[6] + ' ' + input[7] + input[1];
        }
    }]);
})()