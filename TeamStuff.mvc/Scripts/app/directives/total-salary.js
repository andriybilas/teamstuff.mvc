﻿(function () {
    'use strict';

    angular.module('TeamStuff')
        .directive('totalSalary', ['underscore', '$timeout', '$document', '$rootScope', function (_, $timeout, $document, $rootScope) {
            return {
                restrict: 'A',
                transclude: false,
                scope: {
                    ratetype: '=',
                    rate: '=',
                    vacation: '=',
                    sickness: '=',
                    advance: '=',
                    othergrants: '=',
                    reportedhours: '=',
                    grandtotal: '=',
                    workedpay: '='
                },
                link: function (scope, $element) {
                    var that = scope;

                    var updateGrantTotal = function () {
                        if (that.ratetype === 'Monthly') {
                            that.grandtotal = that.workedpay - that.advance + that.othergrants + that.vacation + that.sickness;
                        }

                        if (that.ratetype === "Hourly") {
                            that.workedpay = that.reportedhours * that.rate;
                            that.grandtotal = that.reportedhours * that.rate - that.advance + that.othergrants + that.vacation + that.sickness;
                        }

                        $($element).text(that.grandtotal);

                        $rootScope.$broadcast('updateSummary', 'Broadcast');
                    }

                    scope.$watch('advance', function () {
                        updateGrantTotal();
                    });

                    scope.$watch('othergrants', function () {
                        updateGrantTotal();
                    });

                    scope.$watch('reportedhours', function () {
                        updateGrantTotal();
                    });

                    $($element).text(scope.total);

                }
            }
        }])
        .directive('grandTotalSalary', ['underscore', '$rootScope', function (_, $rootScope) {
            return {
                restrict: 'A',
                transclude: false,
                scope: {
                    employeesCollection: '='
                },
                link: function (scope, $element) {
                    var that = scope;
                    that.total = 0;

                    var updateGrantTotal = function (item) {
                        var total = 0;
                        if (item.salaryRateType === 'Monthly') {
                            total = item.workingSum - item.advance + item.otherGrants + item.vacationSum + item.sicknessSum;
                        }

                        if (item.salaryRateType === "Hourly") {
                            total = item.reportedHours * item.rate - item.advance + item.otherGrants + item.vacationSum + item.sicknessSum;
                        }

                        return total;
                    }

                    $rootScope.$on('updateSummary', function () {
                        var total = 0;

                        _.each(that.employeesCollection, function (item) {
                            total += updateGrantTotal(item);
                        });

                        $($element).text(total);

                    });

                }
            }
        }])
        .directive('totalReport', ['underscore', function (_) {
            return {
                restrict: 'A',
                transclude: false,
                scope: {
                    totalReport: '='
                },
                link: function (scope, $element) {
                    var that = scope;

                    function sumirizeHours() {
                        var sum = 0;
                        _.each(scope.totalReport, function (item) {
                            sum += parseInt(item.hours);
                        });

                        $($element).text(sum);
                    }

                    scope.$watch('totalReport', function () {
                        sumirizeHours();
                    }, true);

                    sumirizeHours();
                }
            }
        }]);
})()