﻿(function () {
    'use strict';

    angular.module('TeamStuff')
        .directive('useDefault', [ function () {
            return {
                restrict: 'A',
                transclude: false,
                scope: {
                    useDefault: '@'
                },
                link: function (scope, $element) {

                    if (!scope.useDefault) {
                        $($element).val(0);
                    }

                    $($element).focusin(function () {
                        if ($(this).val() == 0)
                            $(this).val('');
                    });

                    $($element).blur(function() {
                        if ($(this).val() === '') {
                            $(this).val(0);
                        }
                    });

                }
            }
        }]);
})()