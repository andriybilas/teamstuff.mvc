/// <reference path="../lib/chosen/chosen.jquery.js" />
/// <reference path="../lib/chosen/chosen.jquery.min.js" />
/// <reference path="../lib/chosen/chosen.js" />
/// <reference path="../lib/chosen/chosen.jquery.js" />
/// <reference path="../lib/chosen/chosen.jquery.min.js" />
/// <reference path="../lib/chosen/chosen.js" />
(function () {
    'use strict';
    angular.module('TeamStuff')
        .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }

            $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
            $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

            $urlRouterProvider.otherwise("employees");

            $stateProvider
                .state('employees', {
                    url: '/employees',
                    templateUrl: 'Scripts/app/views/admin/employees.html'
                })
                .state('employeenew', {
                    url: "/new-employee",
                    templateUrl: 'Scripts/app/views/admin/new-employee.html',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'Scripts/lib/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'Scripts/lib/chosen/chosen.jquery.js',
                                        'Scripts/lib/chosen/angular-chosen.js'
                                    ]
                                }
                            ]);
                        }
                    }
                })
                .state('employeeedit', {
                    url: "/edit-employee/:id",
                    templateUrl: 'Scripts/app/views/admin/edit-employee.html',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'Scripts/lib/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'Scripts/lib/chosen/chosen.jquery.js',
                                        'Scripts/lib/chosen/angular-chosen.js'
                                    ]
                                }
                            ]);
                        }
                    }
                })
                .state('rates', {
                    url: "/rates/:employeeId",
                    templateUrl: 'Scripts/app/views/admin/rates.html',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'Scripts/lib/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'Scripts/lib/chosen/chosen.jquery.js',
                                        'Scripts/lib/chosen/angular-chosen.js'
                                    ]
                                }
                            ]);
                        }
                    }
                })
                .state('vacations', {
                    url: "/vacations",
                    templateUrl: 'Scripts/app/views/admin/vacations.html',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'Scripts/lib/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'Scripts/lib/chosen/chosen.jquery.js',
                                        'Scripts/lib/chosen/angular-chosen.js'
                                    ]
                                }
                            ]);
                        }
                    }
                })
                .state('salary', {
                    url: "/salary",
                    templateUrl: 'Scripts/app/views/admin/salary.html'
                })
                .state('reports', {
                    url: "/reports",
                    templateUrl: 'Scripts/app/views/admin/reports.html'
                });
        });

})();

