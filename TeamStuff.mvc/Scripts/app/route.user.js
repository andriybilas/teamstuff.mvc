/// <reference path="../lib/chosen/chosen.jquery.js" />
/// <reference path="../lib/chosen/chosen.jquery.min.js" />
/// <reference path="../lib/chosen/chosen.js" />
/// <reference path="../lib/chosen/chosen.jquery.js" />
/// <reference path="../lib/chosen/chosen.jquery.min.js" />
/// <reference path="../lib/chosen/chosen.js" />
(function () {
    'use strict';
    angular.module('TeamStuff')
        .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }

            $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
            $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

            $urlRouterProvider.otherwise("profile/profile-about");

            $stateProvider
                .state('profile', {
                    url: '/profile',
                    templateUrl: 'Scripts/app/views/user/profile.html'
                })
                .state('profile.profile-about', {
                    url: '/profile-about',
                    templateUrl: 'Scripts/app/views/user/profile-about.html'
                })
                .state('profile.profile-rate', {
                    url: '/profile-rate',
                    templateUrl: 'Scripts/app/views/user/profile-rate.html'
                })
                .state('profile.profile-salary', {
                    url: '/profile-salary',
                    templateUrl: 'Scripts/app/views/user/profile-salary.html',
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'Scripts/lib/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'Scripts/lib/chosen/chosen.jquery.js',
                                        'Scripts/lib/chosen/angular-chosen.js'
                                    ]
                                }
                            ]);
                        }
                    }
                })
                .state('profile.profile-dayoff', {
                    url: '/profile-dayoff',
                    templateUrl: 'Scripts/app/views/user/profile-dayoff.html'
                })
                .state('report', {
                    url: '/report',
                    templateUrl: 'Scripts/app/views/user/report.html',
                });
        });

})();

