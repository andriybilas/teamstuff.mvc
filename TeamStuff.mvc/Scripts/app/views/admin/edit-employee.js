﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('editEmployeesFactory', ['api', function (api) {
        return {
            get: function (id) {
                return api.get('/api/employee', { id: id });
            },
            update : function(data) {
                return api.put('/api/employee', data);
            }
        }
    }])
    .controller('editEmployeeCtrl', ['$scope', '$state', 'editEmployeesFactory', 'toaster', '$stateParams',
        function ($scope, $state, factory, toaster, $stateParams) {
            var vm = this;
            vm.employeeId = $stateParams.id;
            vm.data = {};
            var postFormat = 'YYYY-MM-DD';

            vm.submitForm = function (isValid) {
                vm.submitted = true;
                if (!isValid)
                    return;

                var data = {};
                angular.extend(data, vm.data);

                data.birthday = moment(vm.data.birthday).format(postFormat);
                data.hiredAt = moment(vm.data.hiredAt).format(postFormat);

                if (vm.data.dismissAt)
                    data.dismissAt = moment(vm.data.dismissAt).format(postFormat);

                factory.update(data).then(
                    function (result) {
                        toaster.pop('success', 'Update success', 'Employee were updated');
                        $state.go('employees');
                    });
            };

            vm.goBack = function() {
                $state.go('employees');
            };

            var load = function() {
                factory.get($stateParams.id).then(function (result) {
                    vm.data = result;
                    vm.data.editing = true;
                    vm.data.birthday = new Date(result.birthday);
                    vm.data.hiredAt = new Date(result.hiredAt);

                    if (result.dismissAt)
                        vm.data.dismissAt = new Date(result.dismissAt);
                });
            }

            load();
        }]);
})()