﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('employeesFactory', ['api', function (api) {
        return {
            getAllEmployees: function () {
                return api.get('/api/employee/all');
            }
        }
    }])
    .controller('EmployeesController', ['$scope', 'employeesFactory',
        function ($scope, factory) {
            var vm = this;
            vm.employees = [];

            var getAll = function() {
                factory.getAllEmployees().then(function(result) {
                    vm.employees = result;
                });
            };

            getAll();

            vm.onEdit = function(data) {
                alert(data);
            };

        }]);
})()