﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('newEmployeeFactory', ['api', function (api) {
        return {
            addEmployee: function (data) {
                return api.post('/api/employee', data);
            }
        }
    }])
    .controller('newEmployeeCtrl', ['$scope', 'newEmployeeFactory', 'moment', 'toaster', '$state',
        function ($scope, factory, moment, toaster, $state) {
            var vm = this;
            vm.submitted = false;
            vm.data = {}

            vm.salaryRateRegex = /^\d+$/i;
            vm.data.salaryRateType = 'Monthly';

            vm.submitForm = function (isValid) {
                vm.submitted = true;
                if (!isValid)
                    return;

                var data = {};
                angular.extend(data, vm.data);
                data.birthday = moment(vm.data.birthday).format('YYYY-MM-DD hh:mm:ss');
                data.hiredAt = moment(vm.data.hiredAt).format('YYYY-MM-DD hh:mm:ss');

                if (vm.data.dismissAt)
                    data.dismissAt = moment(vm.data.dismissAt).format('YYYY-MM-DD hh:mm:ss');

                factory.addEmployee(data).then(
                    function (result) {
                        toaster.pop('success', 'Update success', 'Employee were updated');
                        $state.go('employees');
                    });
            };

            vm.goBack = function() {
                $state.go('employees');
            };
        }]);
})()