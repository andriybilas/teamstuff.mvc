﻿(function () {
    'use strict';

angular.module('TeamStuff')
    .factory('ratesFactory', ['api', function (api) {
            return {
                getAll: function(id) {
                    return api.get('/api/rates', { id: id });
                },
                addSalaryRate: function(data) {
                    return api.post('/api/rates', data);
                },
                remove: function(id) {
                    return api.remove('/api/rates', {id: id});
                }
        }
    }])
    .controller('ratesCtrl', ['$scope', 'ratesFactory', '$uibModal', '$stateParams',
        function ($scope, factory, $uibModal, $stateParams) {
            var vm = this;
            vm.employeeId = $stateParams.employeeId;
            vm.submitted = false;
            vm.employee = {}
            vm.dateformat = 'yyyy-MM-dd';

            var getEmployee = function () {
                factory.getAll($stateParams.employeeId).then(function (result) {
                    vm.employee = result;
                });
            };

            vm.open = function (size) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'add-rate.html',
                    controller: 'addRateCtrl',
                    //size: size,
                    backdrop: true,
                    resolve: {
                        employeeId: vm.employee.id
                    }
                });

                modalInstance.result.then(function (selected) {
                    getEmployee();
                }, function () {

                });
            };

            vm.remove = function(id) {
                factory.remove(id).then(function(result) {
                    getEmployee();
                });
            };

            getEmployee();
        }])
    .controller('addRateCtrl', function ($scope, $uibModalInstance, employeeId, ratesFactory, toaster) {

        $scope.data = { employeeId: employeeId, isCurrent: false, rateType: '' };

        $scope.salaryRateTypes = [
            { id: 'Monthly', name: 'Monthly' },
            { id: 'Hourly', name: 'Hourly' }
        ];

        $scope.salaryRateRegex = /^\d+$/i;
        $scope.salaryRateTypeChanged = function(result) {
            $scope.data.rateType = result.name;
            $scope.newSalaryRateForm.salaryRateType.$error.required = false;
        };

        $scope.ok = function (isValid) {
            $scope.submitted = true;
            if (!isValid)
                return;

            if (!$scope.data.rateType) {
                $scope.newSalaryRateForm.salaryRateType = { $error: { required: true } };
                return;
            }

            var data = {};
            angular.extend(data, $scope.data);
            data.applyFrom = moment($scope.data.applyFrom).format('YYYY-MM-DD hh:mm:ss');

            ratesFactory.addSalaryRate(data).then(function (result) {
                toaster.pop('success', "Salary Rate Added", "New salary rate has being added...");
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
})()