(function () {
    'use strict';

    angular
      .module('TeamStuff')
      .factory('reportsFactory', ['api', reportsFactory])
      .controller('reportsController', ['$scope', 'reportsFactory', 'moment', 'constant', reportsController]);

    function reportsFactory(api) {
        return {
            getEmployees: function (period) {
                return api.get('/api/report/employees', { period: period });
            },
            getReportDetail: function(employeeId, period) {
                return api.get('/api/report/detail', { employeeId: employeeId, period: period });
            },
            update: function (report) {
                return api.put('/api/report', report);
            },
            remove: function (id) {
                return api.remove('/api/report', { id: id });
            },
            add: function (report, employeeId) {
                return api.post('/api/report?employeeId=' + employeeId, report);
            }
        }
    };

    function reportsController($scope, factory, moment, constant) {
        var rctrl = this;
        var date = new Date();
        rctrl.reportDetails = [];
        rctrl.reportPeriod = new Date(date.getFullYear(), date.getMonth(), 1);
        rctrl.reportDateOptions = {
            formatYear: 'yy',
            datepickerMode: 'month',
            minMode: 'month'
        };
        var submitEmployeeId = 0;

        rctrl.changeReportPeriod = function() {
            activate();
        };

        rctrl.detalis = function (item) {
            _.each(rctrl.employees, function(employee) {
                employee.expanded = false;
                item.editTemplate = '';
            });

            item.expanded = !item.expanded;

            if (!item.expanded)
                return;

            submitEmployeeId = item.id;

            factory.getReportDetail(item.id, rctrl.reportPeriod).then(function(result) {
                rctrl.reportDetails = result;
                item.expanded = true;
                item.detailTemplate = 'report-detail.html';
            });
        };

        rctrl.addReport = function (isValid) {
            rctrl.submitted = true;
            if (!isValid)
                return;

            rctrl.data.date = moment(rctrl.data.date).format(constant.date.short);

            factory.add(rctrl.data, submitEmployeeId).then(function (result) {
                rctrl.data = { date: new Date(), description: '', hours: '' };
                rctrl.submitted = false;
                activate();
            });

        };

        rctrl.editReportItem = function(item) {
            _.each(rctrl.reportDetails, function (report) {
                report.edited = false;
                report.editTemplate = '';
            });

            item.edited = true;
            rctrl.edited = item;
            rctrl.edited.date = new Date(item.date);
            item.editTemplate = 'Scripts/app/templates/edit-report.html';
        };

        rctrl.editReportItemCancel = function (item) {
            item.edited = false;
            rctrl.edited = null;
            item.editTemplate = '';
        };

        rctrl.remove = function (id) {
            factory.remove(id).then(function (result) {
                activate();
            });
        };

        rctrl.submitEditReport = function (isValid) {
            rctrl.submitEdited = true;
            if (!isValid)
                return;

            rctrl.edited.date = moment(rctrl.edited.date).format(constant.date.short);

            factory.update(rctrl.edited).then(function (result) {
                rctrl.submitEdited = false;
                rctrl.edited.edited = false;
                rctrl.edited.editTemplate = '';
            });
        };

        function activate () {
            var period = moment(rctrl.reportPeriod).format(constant.date.short);
            factory.getEmployees(period).then(function(result) {
                rctrl.employees = result;
            });
        }
        activate();
    }
})();

