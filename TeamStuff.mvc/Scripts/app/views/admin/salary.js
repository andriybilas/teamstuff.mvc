﻿(function () {
    'use strict';

    angular.module('TeamStuff')
        .factory('salaryFactory', [
            'api', function(api) {
                return {
                    calculate: function (salaryDate) {
                        return api.get('/api/salary/calculate', { salaryPeriod : salaryDate});
                    },
                    save: function(data, period) {
                        return api.post('/api/salary?period=' + period, data);
                    },
                    calculatedSalary: function(period) {
                        return api.get('/api/salary/calculated', { period: period });
                    },
                    remove: function(id) {
                        return api.remove('/api/salary', { id: id });
                    }
                }
            }
        ])
        .controller('salaryController', [
            '$scope', 'salaryFactory', 'moment', 'toaster', '$state',
            function($scope, factory, moment, toaster, $state) {
                var vm = this;
                vm.submitted = false;
                vm.employees = [];
                vm.data = {}
                vm.dateformat = 'MMMM-yyyy';
                vm.emploeeys = [];
                vm.salaryTemplate = '';
                vm.period = '';
                vm.salaryPeriod = null;

                vm.maxDateReached = function () {
                    if (vm.salaryPeriod != null) {
                        var now = moment(new Date());
                        var fromNow = moment(vm.salaryPeriod).diff(now, 'month');
                        return fromNow >= 0;
                    }
                        
                    return false;
                };

                vm.dateOptions = {
                    formatYear: 'yy',
                    datepickerMode: 'month',
                    minMode: 'month'
                };

                var calculateSalary = function (period) {
                    factory.calculate(period).then(function (result) {
                        vm.salaryTemplate = 'evaluate-salary.html';
                        vm.emploeeys = result;
                    });
                };

                var calculatedSalary = function() {
                    factory.calculatedSalary(vm.period).then(function (result) {
                        if (result.length !== 0) {
                            vm.salaryTemplate = 'calculated-salary.html';
                            vm.emploeeys = result;
                            return;
                        }

                        calculateSalary(vm.period);
                    });
                };

                $scope.$watch('vm.salaryPeriod', function (newValue, oldValue) {
                    if (newValue) {
                        vm.period = moment(vm.salaryPeriod).format('YYYY-MM-DD hh:mm:ss');
                        calculatedSalary(vm.period);
                    }
                });

                vm.saveChanges = function() {
                    vm.period = moment(vm.salaryPeriod).format('YYYY-MM-DD hh:mm:ss');
                    _.each(vm.emploeeys, function (item) {
                        item.calculationType = 'Complete';
                    });
                    factory.save(vm.emploeeys, vm.period).then(function (result) {
                        toaster.pop('success', 'Success', 'Salary calculation were saved');
                        calculatedSalary(vm.period);
                    });
                };

                vm.saveInterimResults = function() {
                    vm.period = moment(vm.salaryPeriod).format('YYYY-MM-DD hh:mm:ss');
                    _.each(vm.emploeeys, function(item) {
                        item.calculationType = 'Interim';
                    });
                    factory.save(vm.emploeeys, vm.period).then(function (result) {
                        toaster.pop('success', 'Success', 'Saved');
                    });
                };

                vm.recalculateSalary = function() {
                    vm.period = moment(vm.salaryPeriod).format('YYYY-MM-DD hh:mm:ss');
                    calculateSalary(vm.period);
                };

                vm.getPreviosPeriod = function() {
                    vm.salaryPeriod = moment(vm.salaryPeriod).subtract(1, 'months').toDate();
                };

                vm.getNextPeriod = function() {
                    vm.salaryPeriod = moment(vm.salaryPeriod).add(1, 'months').toDate();
                };

                function init() {
                    vm.salaryPeriod = new Date();
                    vm.period = moment(new Date()).format('YYYY-MM-DD hh:mm:ss');
                    calculatedSalary(vm.period);
                }

                init();
            }
        ]);
})()