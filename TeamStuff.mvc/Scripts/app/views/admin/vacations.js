﻿(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('vacationsFactory', ['api', function (api) {
        return {
            getAll: function () {
                return api.get('/api/dayoff/all');
            },
            setVacation: function (data) {
                return api.post('/api/dayoff', data);
            },
            remove: function (id) {
                return api.remove('/api/dayoff', { id: id });
            },
            getAvailableForPeriod: function(period, employeeId) {
                return api.get('/api/dayoff/available', { period: period, employeeId: employeeId });
            }
        }
    }])
    .controller('vacationsController', ['$scope', 'vacationsFactory', 'moment', 'toaster', '$state', '$uibModal', 'underscore',
        function ($scope, factory, moment, toaster, $state, $uibModal, _) {
            var vm = this;
            vm.submitted = false;
            vm.employees = [];
            vm.data = {}
            vm.dateformat = 'yyyy-MM-dd';

            var load = function () {
                factory.getAll().then(function (result) {
                    _.each(result, function(item) {
                        angular.extend(item, { expanded: false });
                    });
                    vm.employees = result;
                });
            };

            vm.setVacation = function (employeeId) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'add-vacation.html',
                    controller: 'vacationModalController',
                    //size: size,
                    resolve: {
                        employeeId: employeeId
                    }
                });

                modalInstance.result.then(function (selected) {
                    load();
                }, function () {

                });
            };

            vm.expandHistory = function (employee, target) {
                _.each(vm.employees, function (item) { item.expanded = false; });

                if (employee.expanded)
                    employee.expanded = false;
                else
                    employee.expanded = true;
            };

            vm.removeMistaken = function(id) {
                factory.remove(id).then(function (result) {
                    toaster.pop('success', 'Success', 'Vacations has being removed');
                    load();
                });
            };

            load();

        }])
    .controller('vacationModalController', function ($scope, $uibModalInstance, employeeId, vacationsFactory, toaster, growlService) {

        $scope.data = { wholeDay: true, employeeId: employeeId, dayOffType: 'Vacation' };

        var getAvailablePaidDaysOff = function () {
            var period = moment($scope.data.from).format('YYYY-MM-DD');
            vacationsFactory.getAvailableForPeriod(period, employeeId).then(function (result) {
                $scope.show = true;
                $scope.result = result;
            });
        }


        $scope.$watch('data.from', function (newVal, oldVal) {
            if (newVal && newVal !== oldVal) {
                getAvailablePaidDaysOff();
            }
        });


        $scope.ok = function (isValid) {
            $scope.submitted = true;
            if (!isValid)
                return;

            if (!$scope.data.dayOffType) {
                $scope.vacationForm.dayOffType = { $error: { required: true } };
                return;
            }

            var data = {};
            angular.extend(data, $scope.data);
            data.from = moment($scope.data.from).format('YYYY-MM-DD hh:mm:ss');
            data.to = moment($scope.data.to).format('YYYY-MM-DD hh:mm:ss');

            vacationsFactory.setVacation(data).then(function (result) {
                toaster.pop('success', "Success", "Day Off added");
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
})()