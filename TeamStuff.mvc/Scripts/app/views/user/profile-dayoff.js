(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('dayOffFactory', ['api', function (api) {
        return {
            get: function () {
                return api.get('/api/profile/dayoff', null);
            }
        }
    }])
    .controller('profileDayOffCtrl', ['$scope', '$state', 'dayOffFactory', 'toaster', '$stateParams',
        function ($scope, $state, factory, toaster, $stateParams) {
            var dctrl = this;
            dctrl.expanded = false;

            dctrl.expandHistory = function() {
                dctrl.expanded = !dctrl.expanded;
            };

            var init = function() {
                factory.get().then(function(result) {
                    dctrl.daysoff = result;
                });
            }

            init();

        }]);
})()