(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('rateFactory', ['api', function (api) {
        return {
            get: function () {
                return api.get('/api/profile/rates', null);
            }
        }
    }])
    .controller('profileRateCtrl', ['$scope', '$state', 'rateFactory', 'toaster', '$stateParams',
        function ($scope, $state, factory, toaster, $stateParams) {
            var rctrl = this;

            var load = function () {
                factory.get().then(function (result) {
                    rctrl.rates = result;
                });
            };

            load();

        }]);
})()