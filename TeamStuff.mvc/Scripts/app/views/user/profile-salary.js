(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('salaryFactory', ['api', function (api) {
        return {
            get: function (year) {
                return api.get('/api/profile/salary', {year: year});
            }
        }
    }])
    .controller('profileSalaryCtrl', ['salaryFactory', 'toaster', 'moment',
        function (factory, toaster, moment) {
            var sctrl = this;
            sctrl.salaryYearCollection = [2015, 2016, 2017];
            sctrl.salaryYear = 2016;

            var init = function (year) {
                factory.get(year).then(function(result) {
                    sctrl.salaries = result;
                });
            };

            sctrl.yearChanged = function () {
                init(sctrl.salaryYear);
            };

            init(moment(new Date()).year());

        }]);
})()