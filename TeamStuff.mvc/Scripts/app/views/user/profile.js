(function () {
    'use strict';

    angular.module('TeamStuff')
    .factory('profileFactory', ['api', function (api) {
        return {
            get: function () {
                return api.get('/api/profile', null);
            },
            updateBaseInfo: function (data) {
                return api.put('/api/profile/update-base', data);
            },
            updateContactInfo: function (data) {
                return api.put('/api/profile/update-contact', data);
            },
            changePassword: function(data) {
                return api.post('/api/profile/update-password', data);
            }
        }
    }])
    .controller('profileCtrl', ['$scope', '$state', 'profileFactory', 'toaster', '$uibModal', 'growlService',
        function ($scope, $state, factory, toaster, $uibModal, growlService) {
            var pctrl = this;
            pctrl.editSummary = 0;
            pctrl.editInfo = 0;
            pctrl.editContact = 0;

            pctrl.submitProfile = function (isValid) {
                pctrl.profileSubmitted = true;
                if (!isValid)
                    return;

                var info = {
                    firstName: pctrl.profile.firstName,
                    lastName: pctrl.profile.lastName,
                    birthday: moment(pctrl.profile.birthday).format('YYYY-MM-DD hh:mm:ss')
                }

                factory.updateBaseInfo(info).then(function (result) {
                    toaster.pop('success', 'Update success', 'Data updated');
                    pctrl.editInfo = 0;
                });
            };

            pctrl.submitContact = function (isValid) {
                pctrl.contactSubmitted = true;
                if (!isValid)
                    return;

                var contact = {
                    mobilePhone: pctrl.profile.mobilePhone,
                    email: pctrl.profile.email,
                    skype: pctrl.profile.skype
                };

                factory.updateContactInfo(contact).then(function (result) {
                    toaster.pop('success', 'Update success', 'Data updated');
                    pctrl.editContact = 0;
                });
            };

            pctrl.accountSettings = function (size) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'change-password.html',
                    controller: 'changePasswordCtrl',
                    backdrop: true,
                    resolve: {
                        userName: function() {
                            return pctrl.profile.userName;
                        }
                    }
                });

                modalInstance.result.then(function (selected) {
                    
                }, function () {

                });
            };

            var load = function() {
                factory.get().then(function (result) {
                    growlService.growl('Hello ' + result.firstName + ' !!!', 'inverse');
                    pctrl.profile = result;
                });
            };

            load();
        }])
        .controller('changePasswordCtrl', function ($scope, $uibModalInstance, profileFactory, toaster, userName) {

            $scope.data = { userName: userName };

            $scope.ok = function (isValid) {
                $scope.submitted = true;
                if (!isValid)
                    return;

                profileFactory.changePassword($scope.data).then(function (result) {
                    toaster.pop('success', "Password successfully changed", "");
                    $uibModalInstance.close();
                });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });;
})()