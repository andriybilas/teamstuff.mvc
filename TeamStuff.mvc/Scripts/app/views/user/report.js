(function () {
    'use strict';
    
    angular.module('TeamStuff')
            .factory('reportFactory', ['api', function (api) {
                return {
                    get: function (period) {
                        return api.get('/api/report', {period: period});
                    },
                    add: function(report) {
                        return api.post('/api/report', report);
                    },
                    update: function(report) {
                        return api.put('/api/report', report);
                    },
                    remove: function(id) {
                        return api.remove('/api/report', { id: id });
                    }
                }
            }])
        .controller('reportCtrl', ['$scope', 'reportFactory', 'underscore', 'constant', function ($scope, factory, _, constant) {
            var rctrl = this;
            var currentDate = new Date();
            rctrl.reports = [];
            rctrl.reportPeriod = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
            rctrl.editReport = 1;
            rctrl.newReport = '';
            rctrl.data = { date: new Date() }
            rctrl.decimalRegex = /^\d+$/i;
            rctrl.editTemplate = '';

            rctrl.reportDateOptions = {
                formatYear: 'yy',
                datepickerMode: 'month',
                minMode: 'month'
            };

            var load = function () {
                var period = moment(rctrl.reportPeriod).format(constant.date.short);
                factory.get(period).then(function (result) {
                    rctrl.reports = result;
                });
            };

            rctrl.changeReportPeriod = function() {
                load();
            };

            rctrl.remove = function(id) {
                factory.remove(id).then(function(result) {
                    load();
                });
            };

            rctrl.editReportItem = function (item) {
                _.each(rctrl.reports, function(report) {
                    report.edited = false;
                    report.editTemplate = '';
                });

                item.edited = true;
                rctrl.edited = item;
                rctrl.edited.date = new Date(item.date);
                item.editTemplate = 'Scripts/app/templates/edit-report.html';
            };

            rctrl.editReportItemCancel = function (item) {
                item.edited = false;
                rctrl.edited = null;
                item.editTemplate = '';
            };

            rctrl.submitEditReport = function(isValid) {
                rctrl.submitEdited = true;
                if (!isValid)
                    return;

                rctrl.edited.date = moment(rctrl.edited.date).format(constant.date.short);

                factory.update(rctrl.edited).then(function (result) {
                    rctrl.submitEdited = false;
                    rctrl.edited.edited = false;
                    rctrl.edited.editTemplate = '';
                    load();
                });
            };

            rctrl.addReport = function(isValid) {
                rctrl.submitted = true;
                if (!isValid)
                    return;

                rctrl.data.date = moment(rctrl.data.date).format(constant.date.short);

                factory.add(rctrl.data).then(function (result) {
                    rctrl.data = { date: new Date(), description: '', hours: '' };
                    rctrl.submitted = false;
                    load();
                });

            };

            load();
        }]);
})()